package org.fejoa.filesystem.fusejnr;

import ru.serce.jnrfuse.FuseFS;

import java.nio.file.Path;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.TimeUnit;

public class Utils {
    static public void blockingMount(final FuseFS stub, final Path tmpDir) throws InterruptedException {
        final CountDownLatch latch = new CountDownLatch(1);
        new Thread(new Runnable() {
            @Override
            public void run() {
                stub.mount(tmpDir, false, true);
                latch.countDown();
            }
        }).start();
        latch.await(1L, TimeUnit.MINUTES);
    }
}
