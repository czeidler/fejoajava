/*
 * Copyright 2017.
 * Distributed under the terms of the GPLv3 License.
 *
 * Authors:
 *      Clemens Zeidler <czei002@aucklanduni.ac.nz>
 */
package org.fejoa.filesystem.fusejnr;

import com.kenai.jffi.MemoryIO;
import jnr.constants.platform.Errno;
import jnr.ffi.Pointer;
import jnr.ffi.types.off_t;
import org.fejoa.library.database.FejoaSeekableByteChannel;
import org.fejoa.library.database.IIOSyncDatabase;
import org.fejoa.library.database.IRandomDataAccess;
import org.fejoa.library.database.StorageDir;
import ru.serce.jnrfuse.FuseFillDir;
import ru.serce.jnrfuse.FuseStubFS;
import ru.serce.jnrfuse.struct.FileStat;
import ru.serce.jnrfuse.struct.FuseFileInfo;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.nio.ByteBuffer;
import java.nio.channels.SeekableByteChannel;
import java.util.Collection;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.atomic.AtomicLong;


public class FejoaFuse extends FuseStubFS {
    static class OpenFilesManager {
        final private AtomicLong handleCounter = new AtomicLong(0);
        final private Map<Long, SeekableByteChannel> openFiles = new ConcurrentHashMap<>();

        private long getNewHandle() {
            return handleCounter.incrementAndGet();
        }

        public SeekableByteChannel getFile(long handle) {
            return openFiles.get(handle);
        }

        public long putFile(SeekableByteChannel channel) {
            Long handle = getNewHandle();
            openFiles.put(handle, channel);
            return handle;
        }

        public void release(Long handle) throws IOException {
            SeekableByteChannel channel = openFiles.get(handle);
            if (channel == null)
                return;

            channel.close();
            openFiles.remove(handle);
        }
    }

    final private StorageDir storageDir;
    final private OpenFilesManager openFilesManager = new OpenFilesManager();

    public FejoaFuse(StorageDir storageDir) {
        this.storageDir = storageDir;
    }

    private int error(Errno errno) {
        return -errno.intValue();
    }

    @Override
    public int getattr(String path, FileStat stat) {
        path = parsePath(path);
        try {
            IIOSyncDatabase.FileType type = storageDir.probe(path);
            if (type == IIOSyncDatabase.FileType.NOT_EXISTING)
                return -error(Errno.ENOENT);
            if (type == IIOSyncDatabase.FileType.FILE) {
                IRandomDataAccess dataAccess = storageDir.open(path, IIOSyncDatabase.Mode.READ);
                stat.st_size.set(dataAccess.length());
                dataAccess.close();
                stat.st_mode.set(FileStat.S_IFREG | 0660);
            } else if (type == IIOSyncDatabase.FileType.DIRECTORY) {
                stat.st_mode.set(FileStat.S_IFDIR | 0755);
            }
        } catch (Exception e) {
            return error(Errno.EIO);
        }
        return 0;
    }

    @Override
    public int readdir(String path, Pointer buf, FuseFillDir filter, @off_t long offset, FuseFileInfo fi) {
        path = parsePath(path);

        filter.apply(buf, ".", null, 0);
        filter.apply(buf, "..", null, 0);
        try {
            Collection<String> dirs = storageDir.listDirectories(path);
            for (String dir : dirs)
                filter.apply(buf, dir, null, 0);
            Collection<String> files = storageDir.listFiles(path);
            for (String file : files)
                filter.apply(buf, file, null, 0);
        } catch (Exception e) {
            return error(Errno.EIO);
        }
        return 0;
    }

    private String parsePath(String path) {
        return path;
    }

    static class FuseFileInfoUtil {
        final static int OPEN_MODE_MASK = 0x003;
        final static int O_RDONLY = 0x0000;
        final static int O_WRONLY =	0x0001;
        final static int O_RDWR	= 0x0002;

        final static int O_CREAT = 0x0100;
        final static int O_TRUNC = 0x01000;
        final static int O_APPEND = 0x02000;

        static public IIOSyncDatabase.Mode getOpenMode(FuseFileInfo fi) {
            int flags = fi.flags.intValue();
            int openMode = (flags & OPEN_MODE_MASK);
            IIOSyncDatabase.Mode mode = null;
            switch (openMode) {
                case O_RDONLY:
                    mode = IIOSyncDatabase.Mode.READ;
                    break;
                case O_WRONLY:
                    mode = IIOSyncDatabase.Mode.WRITE;
                    break;
                case O_RDWR:
                    mode = IIOSyncDatabase.Mode.READ;
                    mode.add(IIOSyncDatabase.Mode.WRITE);
                    break;
            }
            if ((flags & O_CREAT) != 0)
                mode.add(IIOSyncDatabase.Mode.WRITE);
            if ((flags & O_TRUNC) != 0)
                mode.add(IIOSyncDatabase.Mode.TRUNCATE);
            if ((flags & O_APPEND) != 0)
                mode.add(IIOSyncDatabase.Mode.APPEND);
            return mode;
        }
    }

    private int exceptionToError(Exception e) {
        return errnoToError(exceptionToErrno(e));
    }

    private Errno exceptionToErrno(Exception e) {
        if (e instanceof FileNotFoundException)
            return Errno.ENOENT;
        return Errno.EFAULT;
    }

    private int errnoToError(Errno error) {
        return -error.intValue();
    }

    @Override
    public int open(String path, FuseFileInfo fi) {
        path = parsePath(path);
        SeekableByteChannel channel;
        try {
            channel = new FejoaSeekableByteChannel(storageDir.open(path,
                    FuseFileInfoUtil.getOpenMode(fi)));
        } catch (Exception e) {
           return exceptionToError(e);
        }

        Long handle = openFilesManager.putFile(channel);
        fi.fh.set(handle);
        return 0;
    }

    @Override
    public int release(String path, FuseFileInfo fi) {
        try {
            openFilesManager.release(fi.fh.get());
        } catch (IOException e) {
            return exceptionToError(e);
        }
        return 0;
    }

    private static ByteBuffer toByteBuffer(Pointer pointer, long size) {
        if (size >= Integer.MAX_VALUE)
            throw new IllegalArgumentException("Invalid buffer size: " + size);
        return MemoryIO.getInstance().newDirectByteBuffer(pointer.address(), (int)size);
    }

    @Override
    public int read(String path, Pointer buf, long size, long offset, FuseFileInfo fi) {
        SeekableByteChannel channel = openFilesManager.getFile(fi.fh.get());
        if (channel == null)
            return errnoToError(Errno.EINVAL);
        ByteBuffer byteBuffer = toByteBuffer(buf, size);
        try {
            channel.position(offset);
            int byteRead = 0;
            while (byteRead < size) {
                int read = channel.read(byteBuffer);
                if (read <= 0)
                    break;
                byteRead += read;
            }
            return byteRead;
        } catch (IOException e) {
            return exceptionToError(e);
        }
    }

    @Override
    public int write(String path, Pointer buf, long size, long offset, FuseFileInfo fi) {
        SeekableByteChannel channel = openFilesManager.getFile(fi.fh.get());
        if (channel == null)
            return errnoToError(Errno.EINVAL);
        ByteBuffer byteBuffer = toByteBuffer(buf, size);
        try {
            channel.position(offset);
            int byteWritten = 0;
            while (byteWritten < size) {
                int written = channel.write(byteBuffer);
                if (written <= 0)
                    break;
                byteWritten += written;
            }
            return byteWritten;
        } catch (IOException e) {
            return exceptionToError(e);
        }
    }

    @Override
    public int truncate(String path, long size) {
        IRandomDataAccess dataAccess = null;
        try {
            dataAccess = storageDir.open(path, IIOSyncDatabase.Mode.WRITE);
            dataAccess.truncate(size);
            return 0;
        } catch (Exception e) {
            return exceptionToError(e);
        } finally {
            if (dataAccess != null) {
                try {
                    dataAccess.close();
                } catch (Exception e) {
                    return exceptionToError(e);
                }
            }
        }
    }
}
