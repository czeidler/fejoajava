/*
 * Copyright 2017.
 * Distributed under the terms of the GPLv3 License.
 *
 * Authors:
 *      Clemens Zeidler <czei002@aucklanduni.ac.nz>
 */
package org.fejoa.filesystem;

import junit.framework.TestCase;
import org.fejoa.filesystem.fusejnr.FejoaFuse;
import org.fejoa.library.FejoaContext;
import org.fejoa.library.UserData;
import org.fejoa.library.crypto.CryptoException;
import org.fejoa.library.database.StorageDir;
import org.fejoa.library.support.StorageLib;
import org.fejoa.library.support.StreamHelper;
import ru.serce.jnrfuse.FuseFS;

import java.io.*;
import java.nio.file.Path;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.TimeUnit;


public class FejoaFuseTest extends TestCase {
    final List<String> cleanUpDirs = new ArrayList<String>();
    final File baseDir = new File("fejoafs");
    UserData userData;

    @Override
    protected void setUp() throws Exception {
        super.setUp();
        cleanUpDirs.add(baseDir.getName());

        FejoaContext context = new FejoaContext(new File(baseDir, "userData"), null);
        cleanUpDirs.add(context.getHomeDir().getAbsolutePath());
        userData = UserData.create(context, "test");
    }

    @Override
    public void tearDown() throws Exception {
        super.tearDown();

        for (String dir : cleanUpDirs)
            StorageLib.recursiveDeleteFile(new File(dir));
    }

    private StorageDir createStorageDir(String storageContext) throws IOException, CryptoException {
        FejoaContext context = new FejoaContext("UserData", null);
        cleanUpDirs.add(context.getHomeDir().getAbsolutePath());
        final UserData userData = UserData.create(context, "test");
        final StorageDir storageDir = userData.getStorageDir(
                userData.createNewEncryptedStorage(storageContext, "master"));
        userData.commit(true);
        return storageDir;
    }

    private void assertContent(File file, String content) throws IOException {
        String read = new String(StreamHelper.readAll(new FileInputStream(file)));
        assertEquals(content, read);
    }

    protected void blockingMount(final FuseFS stub, final Path tmpDir) throws InterruptedException {
        final CountDownLatch latch = new CountDownLatch(1);
        new Thread(new Runnable() {
            @Override
            public void run() {
                stub.mount(tmpDir, false, true);
                latch.countDown();
            }
        }).start();
        latch.await(1L, TimeUnit.MINUTES);
    }

    public void testBasics() throws Exception {
        StorageDir storageDir = createStorageDir("testContext");
        storageDir.writeString("test", "test");
        storageDir.writeString("dir/test2", "test2");
        storageDir.commit();

        final File mountPoint = new File(baseDir, "fejoaMountPoint");
        mountPoint.mkdirs();
        Path mountDir = mountPoint.getAbsoluteFile().toPath();

        FejoaFuse fileSystenJNR = new FejoaFuse(storageDir);


        blockingMount(fileSystenJNR, mountDir);
        System.out.println("fejoa fs mounted");

        // open a file from the mounted fs and try to edit it
        File mountedTestFile = new File(mountPoint, "dir/test2");
        assertContent(mountedTestFile, "test2");

        FileOutputStream outputStream = new FileOutputStream(mountedTestFile, false);
        outputStream.write("edited".getBytes());
        outputStream.close();

        assertContent(mountedTestFile, "edited");


        // append
        outputStream = new FileOutputStream(mountedTestFile, true);
        outputStream.write("2".getBytes());
        outputStream.close();
        assertContent(mountedTestFile, "edited2");
    }
}

