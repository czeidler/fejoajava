package org.fejoa.cli

import kotlinx.coroutines.experimental.*
import org.fejoa.library.support.StorageLib
import org.junit.After
import org.junit.Test
import java.io.File
import java.util.ArrayList


class DaemonClientTest {
    private val cleanUpDirs = ArrayList<String>()

    @After
    fun cleanUp() {
        for (dir in cleanUpDirs)
            StorageLib.recursiveDeleteFile(File(dir))
    }

    @Test
    fun testInit() {
        val homeDir = File("testAccount")
        cleanUpDirs.add(homeDir.path)
        cleanUp()

        homeDir.mkdirs()

        val client = DaemonClient(DaemonClient.LocalStarter(5 * 1000))
        client!!.exec(arrayOf("init", "-p=password", "-d=${homeDir.path}", "user"))
    }

    @Test
    fun testDaemon() {
        NamedPipe.delete(getPipeFile("daemon.pipe"))
        val daemon = Daemon(1000)

        var client: DaemonClient? = null
        async(newSingleThreadContext("test")) {
            client = DaemonClient(DaemonClient.LocalStarter(5 * 1000))
            client!!.exec(arrayOf("ping", "-n", "5"))
        }

        async(CommonPool) {
            delay(1000)
            client?.cancel()
        }

        runBlocking {
            daemon.join()
        }
    }

    @Test
    fun testPing() {
        var client1: DaemonClient? = null
        var job1 = async(newSingleThreadContext("test")) {
            client1 = DaemonClient(DaemonClient.LocalStarter(5 * 1000))
            client1!!.exec(arrayOf("ping"))
        }

        var client2: DaemonClient? = null
        var job2 = async(newSingleThreadContext("test2")) {
            client2 = DaemonClient(DaemonClient.LocalStarter(5 * 1000))
            client2!!.exec(arrayOf("ping"))
        }

        async(CommonPool) {
            delay(2000)
            client1?.cancel()
            client2?.cancel()
        }

        runBlocking {
            job1.join()
            job2.join()
        }
    }
}
