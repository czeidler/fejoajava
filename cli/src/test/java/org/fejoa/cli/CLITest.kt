package org.fejoa.cli

import com.fasterxml.jackson.databind.ObjectMapper
import org.fejoa.cli.command.ContactCommandResult
import org.fejoa.library.support.StorageLib
import org.fejoa.server.JettyServer
import org.junit.After
import org.junit.Before
import org.junit.Test
import java.io.File
import java.util.ArrayList
import kotlin.system.measureTimeMillis
import kotlin.test.assertEquals
import kotlin.test.assertFalse
import kotlin.test.assertNotNull
import kotlin.test.assertTrue


class CLITest {
    private val cleanUpDirs = ArrayList<String>()

    internal val TEST_DIR = "cliTest"
    internal val HOME_DIR = TEST_DIR + "/User"
    internal val HOME_DIR_2 = TEST_DIR + "/User2"
    internal val SERVER_TEST_DIR = TEST_DIR + "/Server"

    val server = JettyServer(SERVER_TEST_DIR, JettyServer.DEFAULT_PORT)

    @Before
    fun setup() {
        cleanUpDirs.add(TEST_DIR)
        cleanUp()

        File(HOME_DIR).mkdirs()
        File(HOME_DIR_2).mkdirs()
        File(TEST_DIR).mkdirs()

        server.start()
    }

    @After
    fun cleanUp() {
        server.stop()

        for (dir in cleanUpDirs)
            StorageLib.recursiveDeleteFile(File(dir))
    }

    @Test
    fun testOpenCloseTest() {
        val client = DaemonClient(DaemonClient.LocalStarter(10 * 1000), File(HOME_DIR))

        client.exec(arrayOf("init", "-p=password"))
        client.exec(arrayOf("close"))
        assertFalse(client.exec(arrayOf("open", "-p=wrongpassword")) is Done)
        assertTrue(client.exec(arrayOf("open", "-p=password")) is Done)
    }

    private fun waitForContactRequest(client: DaemonClient): String? {
        for (i in 1..100) {
            val result = client.exec(arrayOf("contact")) as Done
            val contactResult = ObjectMapper().readValue(result.returnValue, ContactCommandResult::class.java)
            if (contactResult.contactsRequests.isNotEmpty())
                return contactResult.contactsRequests[0]
            Thread.sleep(100)
        }
        return null
    }

    private fun waitForContact(client: DaemonClient): String? {
        for (i in 1..10) {
            val result = client.exec(arrayOf("contact")) as Done
            val contactResult = ObjectMapper().readValue(result.returnValue, ContactCommandResult::class.java)
            if (contactResult.contacts.isNotEmpty())
                return contactResult.contacts[0]
            Thread.sleep(100)
        }
        return null
    }

    @Test
    fun testClient() {
        val client = DaemonClient(DaemonClient.LocalStarter(60 * 1000), File(HOME_DIR))
        val server = "http://localhost:${JettyServer.DEFAULT_PORT}"
        client.exec(arrayOf("init", "-p=password"))
        client.exec(arrayOf("register", "user1", server))
        client.exec(arrayOf("remote", "add", "-g", "user1", server))
        client.exec(arrayOf("remote"))

        val client2 = DaemonClient(DaemonClient.LocalStarter(60 * 1000), File(HOME_DIR_2))
        client2.exec(arrayOf("init", "-p=password"))
        client2.exec(arrayOf("register", "user2", server))
        client2.exec(arrayOf("remote", "add", "-g", "user2", server))

        //Thread.sleep(2000)
        client.exec(arrayOf("contact", "request", "user2", server))
        val requestedContact = waitForContactRequest(client2) ?: return assertFalse(true)
        client2.exec(arrayOf("contact", "accept", requestedContact))

        waitForContact(client) ?: return assertFalse(true)
        waitForContact(client2) ?: return assertFalse(true)
    }
}