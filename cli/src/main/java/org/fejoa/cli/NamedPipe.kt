package org.fejoa.cli

import kotlinx.coroutines.experimental.*
import kotlinx.coroutines.experimental.channels.Channel
import kotlinx.coroutines.experimental.channels.produce
import kotlinx.coroutines.experimental.selects.select
import org.fejoa.library.support.StreamHelper
import java.io.*
import java.nio.channels.FileChannel
import java.util.concurrent.atomic.AtomicBoolean
import java.util.concurrent.locks.ReentrantLock


class NamedPipe internal constructor() {
    companion object {
        fun create(pipeFile: File) {
            platform.makeNamedPipe(pipeFile)
        }

        fun delete(pipeFile: File) {
            pipeFile.delete()
        }

        fun openForRead(pipeFile: File): InputNamedPipe {
            return InputNamedPipe(pipeFile)
        }

        fun openForWrite(pipeFile: File): OutputNamedPipe {
            val outputData = FileOutputStream(pipeFile)
            return OutputNamedPipe(outputData)
        }

        /**
         * Expects that the pipe exists. Deletes the pipe on timeout to abort the open operation.
         */
        fun openForWrite(pipeFile: File, timeout: Long): OutputNamedPipe {
            if (!pipeFile.exists())
                throw FileNotFoundException()

            fun timeoutFun() = produce(CommonPool) {
                delay(timeout)
                send(true)
            }
            fun writeFun() = produce(newSingleThreadContext("openForWrite")) {
                val fileOutChannel = FileOutputStream(pipeFile)
                send(fileOutChannel)
            }
            val timeoutJob = timeoutFun()
            val writeJob = writeFun()

            return runBlocking {
                select<OutputNamedPipe> {
                    timeoutJob.onReceive {
                        pipeFile.delete()
                        throw CancellationException("Timeout")
                    }
                    writeJob.onReceive { fileOutChannel ->
                        timeoutJob.cancel()
                        OutputNamedPipe(fileOutChannel)
                    }
                }
            }
        }
    }


}

// Workaround for OverlappingFileLockExceptions: only allow one write at a time for any channel on a single VM.
private val localWriteChannelLock : ReentrantLock by lazy { ReentrantLock() }

fun <T>withChannelLock(channel: FileChannel, block: () -> T): T {
    synchronized(localWriteChannelLock) {
        val fileLock = channel.lock()
        try {
            return block.invoke()
        } finally {
            fileLock.release()
        }
    }
}

class OutputNamedPipe(val output: FileOutputStream) {
    fun writeDataPackage(data: ByteArray) {
        return withChannelLock(output.channel) {
            StreamHelper.writeDataPackage(DataOutputStream(output), data)
        }
    }

    fun close() {
        output.close()
    }
}

/**
 * Allows to read from the pipe from a SINGLE process.
 */
class InputNamedPipe(private val pipeFile: File) {
    private var dataInput: DataInputStream? = null
    private val readChannel: Channel<ByteArray> = Channel.invoke(1)
    private val closed = AtomicBoolean(false)

    private val pumpJob = async(newSingleThreadContext("InputNamedPipe: receiver job")) {
        val fileInputStream = FileInputStream(pipeFile)
        dataInput = DataInputStream(fileInputStream)
        // constantly try to read from the pipe otherwise the write thread would block
        while (!closed.get()) {
            try {
                readChannel.send(readDataPackageImp())
            } catch (e: Exception) {
                if (!closed.get())
                    close()
            }
        }
    }

    // The dummy writer keeps the read pump job going. (If all writers are closed the pipe read returns EOF)
    private val dummyWrite: FileOutputStream = FileOutputStream(pipeFile)

    private fun readDataPackageImp(): ByteArray {
        return StreamHelper.readDataPackage(dataInput, 1024 * 1024 * 256)
    }

    fun readDataPackage(): ByteArray {
        return runBlocking { readChannel.receive() }
    }

    fun available(): Boolean {
        return !readChannel.isEmpty
    }

    fun readDataPackage(timeout: Int): ByteArray? {
        if (timeout <= 0)
            return readDataPackage()

        val startTime = System.currentTimeMillis()
        while (!available()) {
            if (System.currentTimeMillis() - startTime >= timeout)
                throw CancellationException("Timeout")
            Thread.sleep(20)
        }
        return readDataPackage()
    }

    fun close() {
        close(null)
    }

    fun close(e: Exception?) {
        if (closed.get())
            return
        closed.set(true)
        try {
            dataInput?.close()
        } catch (e: Exception) {

        }
        if (e == null)
            readChannel.close()
        else
            readChannel.close(e)

        dummyWrite.close()

        runBlocking {
            pumpJob.join()
        }
    }
}
