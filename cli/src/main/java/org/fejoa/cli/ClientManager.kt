package org.fejoa.cli

import kotlinx.coroutines.experimental.asCoroutineDispatcher
import kotlinx.coroutines.experimental.runBlocking
import org.fejoa.library.Client
import org.fejoa.library.ContactPublic
import org.fejoa.library.command.ContactRequestCommandHandler
import org.fejoa.library.command.IncomingCommandManager
import org.fejoa.library.remote.TaskUpdate
import org.fejoa.library.support.LooperExecutor
import org.fejoa.library.support.Task
import java.io.File
import java.util.concurrent.ConcurrentHashMap
import java.util.concurrent.ExecutorService
import java.util.concurrent.Executors

/**
 * Manages all open clients
 */
class ClientManager {
    class ClientEntry(val client: Client) {
        private val mountManager: MountManager = MountManager(client.userData)

        fun close() {
            mountManager.umount()
            client.close()
        }
    }

    /**
     * Path -> Client
     */
    private val openClients: MutableMap<String, ClientEntry> = ConcurrentHashMap()
    private val executor: ExecutorService = Executors.newCachedThreadPool()

    fun open(homeDir: File, password: String): Client {
        val homeDirPath = homeDir.absolutePath
        if (openClients.containsKey(homeDirPath))
            throw Exception("Abort open client, already exist for $homeDir")
        val client = Client.open(homeDir, LooperExecutor(executor), password)
        start(client)
        openClients[homeDirPath] = ClientEntry(client)
        return client
    }

    fun init(homeDir: File, password: String): Client {
        val client = Client.init(homeDir, LooperExecutor(executor), password)
        client.commit()
        start(client)
        openClients[homeDir.absolutePath] = ClientEntry(client)
        return client
    }

    fun getOpenClients(): Map<String, ClientEntry> = openClients

    fun getOpenClient(path: String): Client? = openClients[path]?.client

    private fun close(entry: ClientEntry) {
        runBlocking(entry.client.contextExecutor.asCoroutineDispatcher()) {
            entry.close()
        }
    }

    /**
     * @return false if no client for the given homeDir is open
     */
    fun close(homeDir: String): Boolean {
        val entry = openClients.remove(homeDir) ?: return false
        close(entry)
        return true
    }

    fun close() {
        openClients.forEach {_, entry -> close(entry) }
        while (openClients.isNotEmpty()) {
            val toBeClosed = openClients.map {
                it.key
            }
            toBeClosed.forEach {
                close(it)
            }
        }

        executor.shutdown()
    }

    private fun start(client: Client) {
        client.contactManager.setContactRequestListener(object : ContactRequestCommandHandler.IListener {
            override fun onContactRequest(contactRequest: ContactRequestCommandHandler.ContactRequest?) {

            }

            override fun onContactRequestReply(contactRequest: ContactRequestCommandHandler.ContactRequest) {
                // auto accept the request reply
                contactRequest.accept()
            }

            override fun onContactRequestFinished(contactPublic: ContactPublic?) {

            }

            override fun onError(e: Exception?) {

            }

        })
        val dummyObserver = object: Task.IObserver<TaskUpdate, Void>{
            override fun onProgress(update: TaskUpdate?) {

            }

            override fun onResult(result: Void?) {

            }

            override fun onException(exception: java.lang.Exception?) {

            }
        }
        client.startSyncing(dummyObserver)
        client.startCommandManagers(dummyObserver)
    }
}
