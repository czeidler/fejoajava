package org.fejoa.cli

import org.fejoa.filesystem.fusejnr.FejoaFuse
import org.fejoa.filesystem.fusejnr.Utils
import org.fejoa.library.BranchInfo
import org.fejoa.library.UserData
import ru.serce.jnrfuse.FuseFS
import java.io.File
import java.io.IOException


class MountManager(private val userData: UserData) {
    private class Mount(private val fuseFs: FuseFS, val branch: String, val description: String) {
        fun umount() {
            fuseFs.umount()
        }
    }

    // path -> Mount
    private val mountMap: MutableMap<String, Mount> = HashMap()

    fun mount(branchInfo: BranchInfo, path: String) {
        val dirName = "${branchInfo.branch} (${branchInfo.description.replace(" ", "_")})"
        val mountPoint = File(path, dirName)
        mountPoint.mkdirs()
        if (mountPoint.list().isNotEmpty())
            throw IOException("Mount point ${mountPoint.path} is not empty")
        val mountDir = mountPoint.absoluteFile.toPath()

        val storageDir = userData.getStorageDir(branchInfo)
        val fileSystem = FejoaFuse(storageDir)

        Utils.blockingMount(fileSystem, mountDir)

        mountMap[path] = Mount(fileSystem, storageDir.branch, branchInfo.description)
    }

    fun umount() {
        mountMap.forEach { it.value.umount() }
        mountMap.clear()
    }

    fun umount(path: String) {
        mountMap.remove(path)?.let { it.umount() }
    }
}
