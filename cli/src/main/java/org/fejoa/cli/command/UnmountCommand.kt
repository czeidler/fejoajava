package org.fejoa.cli.command

import com.fasterxml.jackson.annotation.JsonInclude
import com.fasterxml.jackson.databind.ObjectMapper
import org.fejoa.cli.ArgsBase
import org.fejoa.cli.Daemon
import org.fejoa.cli.IClientCommand
import org.fejoa.library.Client
import picocli.CommandLine


const val UNMOUNT_COMMAND_NAME = "unmount"

class UnmountClientCommand : IClientCommand {
    @CommandLine.Command(name = UNMOUNT_COMMAND_NAME, description = arrayOf("Unmount data branches"))
    class UnmountArgs : ArgsBase()

    override fun createArgs(): Any = UnmountArgs()

    override fun start(argObject: Any): String? {
        return ObjectMapper().setSerializationInclusion(JsonInclude.Include.NON_NULL).writeValueAsString(argObject)
    }
}

class UnmountDaemonJob(daemon: Daemon) : BaseDaemonJob(daemon) {
    suspend override fun runInternal(client: Client, path: String, jsonArgs: String?): Any? {

        return null
    }
}
