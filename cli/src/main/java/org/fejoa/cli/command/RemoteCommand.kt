package org.fejoa.cli.command

import com.fasterxml.jackson.annotation.JsonInclude
import com.fasterxml.jackson.databind.ObjectMapper
import org.fejoa.cli.ArgsBase
import org.fejoa.cli.Daemon
import org.fejoa.cli.DaemonJob
import org.fejoa.cli.IClientCommand
import org.fejoa.library.Client
import picocli.CommandLine

const val REMOTE_COMMAND_NAME = "remote"

class RemoteCommand : IClientCommand {
    @CommandLine.Command(name = REMOTE_COMMAND_NAME, description = arrayOf("Manage remotes (CSPs)"))
    class RemoteArgs : ArgsBase()

    override fun createArgs(): Any = RemoteArgs()

    override fun start(argObject: Any): String? {
        assert(argObject is RemoteArgs)
        return ObjectMapper().setSerializationInclusion(JsonInclude.Include.NON_NULL).writeValueAsString(argObject)
    }
}

class RemoteDaemonJob(daemon: Daemon) : BaseDaemonJob(daemon) {
    suspend override fun runInternal(client: Client, path: String, jsonArgs: String?): Any? {
        client.userData.remoteStore.entries.forEach {
            reportProgress(it.toAddress())
        }
        return null
    }
}