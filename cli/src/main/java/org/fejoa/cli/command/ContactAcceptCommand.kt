package org.fejoa.cli.command

import com.fasterxml.jackson.annotation.JsonInclude
import com.fasterxml.jackson.databind.ObjectMapper
import org.fejoa.cli.ArgsBase
import org.fejoa.cli.Daemon
import org.fejoa.cli.IClientCommand
import org.fejoa.library.Client
import picocli.CommandLine


const val CONTACT_ACCEPT_COMMAND_NAME = "accept"

class ContactAcceptClientCommand : IClientCommand {
    @CommandLine.Command(name = CONTACT_ACCEPT_COMMAND_NAME, description = arrayOf("Accept a contact request"))
    class ContactAcceptArgs : ArgsBase() {
        @CommandLine.Parameters(index = "0", description = arrayOf("contact id"))
        var contactId: String = ""
    }

    override fun createArgs(): Any = ContactAcceptArgs()

    override fun start(argObject: Any): String? {
        return ObjectMapper().setSerializationInclusion(JsonInclude.Include.NON_NULL).writeValueAsString(argObject)
    }
}

class ContactAcceptDaemonJob(daemon: Daemon) : BaseDaemonJob(daemon) {
    suspend override fun runInternal(client: Client, path: String, jsonArgs: String?): Any? {
        val mapper = ObjectMapper()
        val argsObject : ContactAcceptClientCommand.ContactAcceptArgs
                = mapper.readValue(jsonArgs.toString(), ContactAcceptClientCommand.ContactAcceptArgs::class.java)

        val contactRequest = client.contactManager.getContactRequests().firstOrNull {
            it.contact.id == argsObject.contactId
        } ?: return failJob("Not contact request for contact ${argsObject.contactId} found")
        contactRequest.accept()
        reportProgress("Contact ${argsObject.contactId} accepted")
        return null
    }
}



