package org.fejoa.cli.command

import com.fasterxml.jackson.annotation.JsonInclude
import com.fasterxml.jackson.databind.ObjectMapper
import kotlinx.coroutines.experimental.asCoroutineDispatcher
import kotlinx.coroutines.experimental.runBlocking
import org.fejoa.cli.ArgsBase
import org.fejoa.cli.Daemon
import org.fejoa.cli.DaemonJob
import org.fejoa.cli.IClientCommand
import org.fejoa.library.Client
import org.fejoa.library.UserDataSettings
import picocli.CommandLine
import java.io.File


const val STATUS_COMMAND_NAME = "status"

class StatusClientCommand : IClientCommand {
    @CommandLine.Command(name = STATUS_COMMAND_NAME, description = arrayOf("Query the current client status"))
    class StatusArgs : ArgsBase()

    override fun createArgs(): Any = StatusArgs()

    override fun start(argObject: Any): String? {
        return ObjectMapper().setSerializationInclusion(JsonInclude.Include.NON_NULL).writeValueAsString(argObject)
    }
}

class StatusDaemonJob(private val daemon: Daemon) : DaemonJob() {
    suspend override fun runInternal(path: String, args: String?): Any? {
        daemon.clientManager.getOpenClient(path)?.let {
            // open
            return runBlocking(it.contextExecutor.asCoroutineDispatcher()) {
                reportProgress("Client open for user: " + it.userData.id)
            }
        }

        try {
            // account files exist?
            val settings: UserDataSettings = Client.readUserDataSettings(Client.getUserDataSettingsFile(File(path)))
            return reportProgress("Unopened account found. Type \"fejoa open\" to open the account")
        } catch (e: Exception) {
            return reportProgress("No account at: $path")
        }
    }
}
