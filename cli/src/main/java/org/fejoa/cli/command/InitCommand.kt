package org.fejoa.cli.command

import picocli.CommandLine
import java.io.File
import com.fasterxml.jackson.databind.ObjectMapper
import org.fejoa.cli.ArgsBase
import org.fejoa.cli.Daemon
import org.fejoa.cli.DaemonJob
import org.fejoa.cli.IClientCommand
import java.io.Console


const val INIT_COMMAND_NAME = "init"

//Fejoa init -h=homeDir, -p password
@CommandLine.Command(name = INIT_COMMAND_NAME, description = arrayOf("Initialize a new local Fejoa account"))
class InitArgs : ArgsBase() {
    @CommandLine.Option(names = arrayOf("-p", "--password"),
            description = arrayOf("password"))
    var password: String = ""
}

class InitClientCommand : IClientCommand {
    override fun createArgs(): Any = InitArgs()

    override fun start(argsObject: Any): String? {
        assert(argsObject is InitArgs)
        val args = argsObject as InitArgs
        if (args.password == null) {
            val console: Console = System.console() ?: return null
            val password1 = console.readPassword("Enter password").contentToString()
            if (!validatePassword(password1))
                throw IllegalArgumentException("Password too simple")
            val password2 = console.readPassword("Re-enter password").contentToString()
            if (password1 != password2)
                throw IllegalArgumentException("Password mismatch")
            args.password = password1
        }
        val mapper = ObjectMapper()
        return mapper.writeValueAsString(args)
    }
}

class InitDaemonJob(val daemon: Daemon) : DaemonJob() {
    suspend override fun runInternal(path: String, args: String?): Any? {
        val mapper = ObjectMapper()
        val argsObject : InitArgs = mapper.readValue(args.toString(), InitArgs::class.java)

        if (daemon.clientManager.getOpenClients().containsKey(path))
            return failJob("Account already exist at \"$path\"")
        val dir = File(path)
        if (!dir.isDirectory)
            return failJob("Internal error: path \"$path\" must be a directory")
        if (dir.list().isNotEmpty())
            return failJob("Directory is not empty; use an empty directory to init a new account")

        reportProgress("Create new account...")

        val client = daemon.clientManager.init(dir, argsObject.password)
        return reportProgress("done (id ${client.userData.id})")
    }
}
