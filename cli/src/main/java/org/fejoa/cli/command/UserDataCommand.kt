package org.fejoa.cli.command

import com.fasterxml.jackson.annotation.JsonInclude
import com.fasterxml.jackson.databind.ObjectMapper
import org.fejoa.cli.ArgsBase
import org.fejoa.cli.Daemon
import org.fejoa.cli.IClientCommand
import org.fejoa.library.Client
import picocli.CommandLine


const val USER_DATA_COMMAND_NAME = "userdata"

class UserDataClientCommand : IClientCommand {
    @CommandLine.Command(name = USER_DATA_COMMAND_NAME, description = arrayOf("List user data info"))
    class UserDataArgs : ArgsBase()

    override fun createArgs(): Any = UserDataArgs()

    override fun start(argObject: Any): String? {
        return ObjectMapper().setSerializationInclusion(JsonInclude.Include.NON_NULL).writeValueAsString(argObject)
    }
}

class UserDataDaemonJob(daemon: Daemon) : BaseDaemonJob(daemon) {
    suspend override fun runInternal(client: Client, path: String, jsonArgs: String?): Any? {
        client.userData.branchList.getEntries(true).forEach {
            reportProgress("${it.branch}: ${it.description}")
        }
        return null
    }
}
