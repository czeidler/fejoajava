package org.fejoa.cli.command

import com.fasterxml.jackson.annotation.JsonInclude
import com.fasterxml.jackson.databind.ObjectMapper
import org.fejoa.cli.ArgsBase
import org.fejoa.cli.Daemon
import org.fejoa.cli.IClientCommand
import org.fejoa.library.Client
import picocli.CommandLine


const val CONTACT_COMMAND_NAME = "contact"

class ContactClientCommand : IClientCommand {
    @CommandLine.Command(name = CONTACT_COMMAND_NAME, description = arrayOf("Manage contacts"))
    class ContactArgs : ArgsBase()

    override fun createArgs(): Any = ContactArgs()

    override fun start(argObject: Any): String? {
        return ObjectMapper().setSerializationInclusion(JsonInclude.Include.NON_NULL).writeValueAsString(argObject)
    }
}

class ContactCommandResult {
    val contacts: MutableList<String> = ArrayList()
    val requestedContacts: MutableList<String> = ArrayList()
    val contactsRequests: MutableList<String> = ArrayList()
}

class ContactDaemonJob(daemon: Daemon) : BaseDaemonJob(daemon) {
    suspend override fun runInternal(client: Client, path: String, jsonArgs: String?): Any? {
        val result = ContactCommandResult()

        val contactManager = client.contactManager
        val contacts = contactManager.getContacts()
        val requestedContacts = contactManager.getRequestedContacts()
        val contactRequests = contactManager.getContactRequests()

        result.contacts.addAll(contacts.map { it.id })
        result.requestedContacts.addAll(requestedContacts.map { it.id })
        result.contactsRequests.addAll(contactRequests.map { it.contact.id })

        if (contacts.isNotEmpty())
            reportProgress("Contacts:")
        contacts.forEach {
            var entry = "${it.id}";
            it.remotes.entries.forEach {
                entry += " ${it.toAddress()}"
            }
            reportProgress(entry)
        }

        if (requestedContacts.isNotEmpty())
            reportProgress("Requested contacts:")
        requestedContacts.forEach {
            var entry = "${it.id}";
            it.remotes.entries.forEach {
                entry += " ${it.toAddress()}"
            }
            reportProgress(entry)
        }

        if (contactRequests.isNotEmpty())
            reportProgress("Contact requests:")
        contactRequests.forEach {
            var entry = "${it.contact.id}";
            it.contact.remotes.entries.forEach {
                entry += " ${it.toAddress()}"
            }
            reportProgress(entry)
        }
        return result
    }
}



