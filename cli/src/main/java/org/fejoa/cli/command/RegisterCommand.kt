package org.fejoa.cli.command

import com.fasterxml.jackson.annotation.JsonInclude
import com.fasterxml.jackson.databind.ObjectMapper
import java8.util.concurrent.CompletableFuture
import org.fejoa.cli.*
import org.fejoa.library.Client
import org.fejoa.library.Remote
import org.fejoa.library.remote.RegisterAccountJob
import org.fejoa.library.remote.Errors
import org.fejoa.library.support.await
import picocli.CommandLine
import javax.crypto.SecretKey


const val REGISTER_COMMAND_NAME = "register"

@CommandLine.Command(name = PING_COMMAND_NAME, description = arrayOf("Register an account at a CSP"))
class RegisterArgs : ArgsBase() {
    @CommandLine.Parameters(index = "0", description = arrayOf("user name"))
    var userName: String? = null

    @CommandLine.Parameters(index = "1", description = arrayOf("server"))
    var server: String? = null
}

class RegisterCommand : IClientCommand {
    override fun createArgs(): Any {
        return RegisterArgs()
    }

    override fun start(argObject: Any): String? {
        assert(argObject is RegisterArgs)
        return ObjectMapper().setSerializationInclusion(JsonInclude.Include.NON_NULL).writeValueAsString(argObject)
    }
}

class RegisterDaemonJob(daemon: Daemon) : BaseDaemonJob(daemon) {
    override suspend fun runInternal(client: Client, path: String, jsonArgs: String?): Any? {
        val mapper = ObjectMapper()
        val args : RegisterArgs = mapper.readValue(jsonArgs.toString(), RegisterArgs::class.java)
        var baseKey: SecretKey? = client.baseKey

        val remote = Remote(args.userName, args.server)
        val job: CompletableFuture<RegisterAccountJob.Result>
        job = if (baseKey == null) {
            val password1 = sendRequest(InputRequest("Enter registration password", true))
            if (!validatePassword(password1))
                return failJob("Password too simple")
            val password2 = sendRequest(InputRequest("Re-enter registration password", true))
            if (password1 != password2)
                return failJob("Password mismatch")
            client.registerAccount(remote, password1)
        } else
            client.registerAccount(remote, baseKey)
        val result = job.await()
        if (result.status != Errors.DONE)
            return failJob(result.message)
        reportProgress("Registered to ${args.userName}@${args.server}")
        return result
    }
}