package org.fejoa.cli.command

import kotlinx.coroutines.experimental.asCoroutineDispatcher
import kotlinx.coroutines.experimental.async
import org.fejoa.cli.Daemon
import org.fejoa.cli.DaemonJob
import org.fejoa.library.Client

/**
 * Base job for jobs that requires an open client
 */
abstract class BaseDaemonJob(val daemon: Daemon) : DaemonJob() {
    suspend override fun runInternal(path: String, args: String?): Any? {
        val client = daemon.clientManager.getOpenClient(path) ?: return failJob("Client need to be open")
        return async(client.contextExecutor.asCoroutineDispatcher()) {
            runInternal(client, path, args)
        }.await()
    }

    /**
     * Is run in the client executor context
     */
    abstract suspend protected fun runInternal(client: Client, path: String, jsonArgs: String?): Any?
}
