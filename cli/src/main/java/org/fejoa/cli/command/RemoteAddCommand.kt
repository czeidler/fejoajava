package org.fejoa.cli.command

import com.fasterxml.jackson.annotation.JsonInclude
import com.fasterxml.jackson.databind.ObjectMapper
import kotlinx.coroutines.experimental.CoroutineStart
import kotlinx.coroutines.experimental.async
import org.fejoa.cli.*
import org.fejoa.library.Client
import org.fejoa.library.Remote
import org.fejoa.library.UserData
import org.fejoa.library.remote.AuthInfo
import org.fejoa.library.remote.Errors
import org.fejoa.library.remote.PasswordProvider
import org.fejoa.library.support.Task
import org.fejoa.library.support.await
import picocli.CommandLine

const val REMOTE_ADD_COMMAND_NAME = "add"

@CommandLine.Command(name = REMOTE_ADD_COMMAND_NAME, description = arrayOf("Add a new remote CSP"))
class RemoteAddArgs : ArgsBase() {
    @CommandLine.Option(names = arrayOf("-g", "gateway"), description = arrayOf("Make the remote the current gateway"),
            required = false)
    var gateway: Boolean = false

    @CommandLine.Parameters(index = "0", description = arrayOf("user name"))
    var userName: String? = null

    @CommandLine.Parameters(index = "1", description = arrayOf("server"))
    var server: String? = null
}

class RemoteAddCommand : IClientCommand {

    override fun createArgs(): Any = RemoteAddArgs()

    override fun start(argObject: Any): String? {
        assert(argObject is RemoteAddArgs)
        return ObjectMapper().setSerializationInclusion(JsonInclude.Include.NON_NULL).writeValueAsString(argObject)
    }
}

class RemoteAddDaemonJob(daemon: Daemon) : BaseDaemonJob(daemon) {
    suspend override fun runInternal(client: Client, path: String, jsonArgs: String?): Any? {
        val mapper = ObjectMapper()
        val args : RemoteAddArgs = mapper.readValue(jsonArgs.toString(), RemoteAddArgs::class.java)
        val remote = Remote(args.userName, args.server)
        val addRemoteResult = client.addRemote(remote, args.gateway,
                object: PasswordProvider(Task.CurrentThreadScheduler()) {
            override fun getPassword(request: Request) {
                async(CommonCachedPool, CoroutineStart.UNDISPATCHED) {
                    val password = sendRequest(InputRequest())
                    request.setPassword(password)
                }
            }
        }).await()

        if (addRemoteResult.status != Errors.DONE)
            return failJob(addRemoteResult.message)

        if (args.gateway) {
            client.setBranchLocation(UserData.USER_DATA_CONTEXT, remote,
                    AuthInfo.KeyStoreLogin(remote, client.userData.keyStore))
        }
        client.commit()
        return addRemoteResult
    }
}