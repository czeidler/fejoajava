package org.fejoa.cli.command

import com.fasterxml.jackson.annotation.JsonInclude
import com.fasterxml.jackson.databind.ObjectMapper
import org.fejoa.cli.ArgsBase
import org.fejoa.cli.Daemon
import org.fejoa.cli.IClientCommand
import org.fejoa.filesystem.fusejnr.FejoaFuse
import org.fejoa.filesystem.fusejnr.Utils
import org.fejoa.library.Client
import picocli.CommandLine
import java.io.File


const val MOUNT_COMMAND_NAME = "mount"

class MountClientCommand : IClientCommand {
    @CommandLine.Command(name = MOUNT_COMMAND_NAME, description = arrayOf("Mount data branches"))
    class MountArgs : ArgsBase() {
        @CommandLine.Parameters(index = "0", description = arrayOf("branch"))
        var branch: String = ""
    }

    override fun createArgs(): Any = MountArgs()

    override fun start(argObject: Any): String? {
        return ObjectMapper().setSerializationInclusion(JsonInclude.Include.NON_NULL).writeValueAsString(argObject)
    }
}

class MountDaemonJob(daemon: Daemon) : BaseDaemonJob(daemon) {
    suspend override fun runInternal(client: Client, path: String, jsonArgs: String?): Any? {
        val args = ObjectMapper().readValue(jsonArgs, MountClientCommand.MountArgs::class.java)
        val branchInfo = client.userData.branchList.getEntries(true).firstOrNull {
            it.branch.startsWith(args.branch)
        } ?: return failJob("Branch not found")

        // TODO move this into a MountManager
        val dirName = "${branchInfo.branch} (${branchInfo.description.replace(" ", "_")})"
        val mountPoint = File(path, dirName)
        mountPoint.mkdirs()
        if (mountPoint.list().isNotEmpty())
            return failJob("Mount point ${mountPoint.path} is not empty")
        val mountDir = mountPoint.absoluteFile.toPath()

        val storageDir = client.userData.getStorageDir(branchInfo)
        val fileSystenJNR = FejoaFuse(storageDir)

        Utils.blockingMount(fileSystenJNR, mountDir)
        return reportProgress("Branch ${branchInfo.branch} mounted at $dirName")
    }
}
