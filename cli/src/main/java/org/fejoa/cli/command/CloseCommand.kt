package org.fejoa.cli.command

import com.fasterxml.jackson.annotation.JsonInclude
import com.fasterxml.jackson.databind.ObjectMapper
import org.fejoa.cli.ArgsBase
import org.fejoa.cli.Daemon
import org.fejoa.cli.DaemonJob
import org.fejoa.cli.IClientCommand
import picocli.CommandLine


const val CLOSE_COMMAND_NAME = "close"

class CloseClientCommand : IClientCommand {
    @CommandLine.Command(name = CLOSE_COMMAND_NAME, description = arrayOf("Close an open account"))
    class CloseArgs : ArgsBase()

    override fun createArgs(): Any = CloseArgs()

    override fun start(argObject: Any): String? {
        return ObjectMapper().setSerializationInclusion(JsonInclude.Include.NON_NULL).writeValueAsString(argObject)
    }
}

class CloseDaemonJob(private val daemon: Daemon) : DaemonJob() {
    suspend override fun runInternal(path: String, args: String?): Any? {
        if (!daemon.clientManager.close(path)) {
            return reportProgress("Client closed")
        }
        reportProgress("Client not open")
        return null
    }
}



