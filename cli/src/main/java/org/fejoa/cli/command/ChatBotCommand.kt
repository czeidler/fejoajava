package org.fejoa.cli.command

import com.fasterxml.jackson.databind.ObjectMapper
import org.fejoa.cli.*
import picocli.CommandLine

const val CHAT_BOT_COMMAND_NAME = "chatbot"


@CommandLine.Command(name = CHAT_BOT_COMMAND_NAME, description = arrayOf("CLI interactive demo"))
private class ChatBotArgs : ArgsBase()

class ChatBotCommand : IClientCommand {
    override fun createArgs(): Any {
        return ChatBotArgs()
    }

    override fun start(argsObject: Any): String? {
        return null
    }
}


class ChatBotJob : DaemonJob() {
    suspend override fun runInternal(path: String, args: String?): Any? {
        reportProgress("Welcome to the Fejoa ChatBot (interactive CLI demo)")
        val mapper = ObjectMapper()
        loop@ while (true) {
            var response = sendRequest(InputRequest("> "))
            val inputReply = mapper.readValue(response, InputReply::class.java)
            val message = inputReply.reply ?: continue
            when (message.trim()) {
                "help" -> reportProgress("Type \"quit\" to exit")
                "quit" -> {
                    reportProgress("bye bye")
                    break@loop
                }
                else -> reportProgress("You said: $message")
            }
        }
        return null
    }
}