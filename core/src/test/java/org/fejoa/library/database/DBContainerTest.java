/*
 * Copyright 2016.
 * Distributed under the terms of the GPLv3 License.
 *
 * Authors:
 *      Clemens Zeidler <czei002@aucklanduni.ac.nz>
 */
package org.fejoa.library.database;

import junit.framework.TestCase;

import java.util.*;
import java.util.concurrent.ExecutionException;


public class DBContainerTest extends TestCase {
    class TestContainerObject extends DBObjectContainer {
        final public DBString value;

        public TestContainerObject() {
            value = new DBString("value");
            add(value);
        }
    }

    public void testBasics() throws ExecutionException, InterruptedException {
        IOStorageDir dir = new IOStorageDir(AsyncInterfaceUtil.fakeAsync(new MemoryIODatabase()), "");

        DBObjectContainer root = new DBObjectContainer();

        DBString value1 = new DBString("value1");
        value1.set("1");
        DBString value2 = new DBString("value2");
        value2.set("2");

        root.add(value1);
        root.add(value2);

        DBObjectContainer subContainer = new DBObjectContainer();
        root.add(subContainer, "sub");

        DBString value3= new DBString("value3");
        value3.set("3");
        subContainer.add(value3);

        root.setTo(dir);
        root.flush().get();

        assertEquals("1", new DBString(dir, "value1").read().get());
        assertEquals("2", new DBString(dir, "value2").read().get());
        assertEquals("3", new DBString(dir, "sub/value3").read().get());

        DBObjectList<DBString> fileList = new DBObjectList<>(new DBObjectList.FileBackend() {
            @Override
            public DBString create(String entryName) {
                return new DBString();
            }
        });

        fileList.get("entry1").set("Entry1");
        fileList.get("entry2").set("Entry2");

        root.add(fileList, "list");
        root.flush().get();

        assertEquals(2, fileList.getDirContent().read().get().size());
        assertEquals("Entry1", new DBString(dir, "list/entry1").read().get());
        assertEquals("Entry2", new DBString(dir, "list/entry2").read().get());

        DBObjectList<TestContainerObject> dirObjectList = new DBObjectList<>(new DBObjectList.DirBackend() {
            @Override
            public TestContainerObject create(String entryName) {
                return new TestContainerObject();
            }
        });
        dirObjectList.get("entry3").value.set("Entry3");
        dirObjectList.get("entry4").value.set("Entry4");
        subContainer.add(dirObjectList, "list2");
        subContainer.flush().get();

        assertEquals("Entry3", new DBString(dir, "sub/list2/entry3/value").read().get());
        assertEquals("Entry4", new DBString(dir, "sub/list2/entry4/value").read().get());

        subContainer.invalidate();

        root = new DBObjectContainer(dir);
        subContainer = new DBObjectContainer();
        root.add(subContainer, "sub");
        dirObjectList = new DBObjectList<>(new DBObjectList.DirBackend() {
            @Override
            public TestContainerObject create(String entryName) {
                return new TestContainerObject();
            }
        });
        subContainer.add(dirObjectList, "list2");

        assertEquals("Entry3", dirObjectList.get("entry3").value.read().get());
        assertEquals("Entry4", dirObjectList.get("entry4").value.read().get());
        Collection<String> content = dirObjectList.getDirContent().read().get();
        assertEquals(2, content.size());
    }

    public void testSplitFileList() throws Exception {
        IOStorageDir dir = new IOStorageDir(AsyncInterfaceUtil.fakeAsync(new MemoryIODatabase()), "");

        dir.putBytes("ab/test1", "test1".getBytes());
        dir.putBytes("ab/test2", "test1".getBytes());
        dir.putBytes("ac/test3", "test1".getBytes());
        dir.putBytes("ad/test4", "test1".getBytes());

        DBObjectList<DBString> splitList = new DBObjectList<>(new DBObjectList.SplitFileBackend() {
            @Override
            public IDBContainerEntry create(String entryName) {
                return new DBString();
            }
        });
        splitList.setTo(dir);

        Collection<String> content = splitList.getDirContent().read().join();
        assertEquals(4, content.size());
        assertTrue(content.contains("abtest1"));
        assertTrue(content.contains("abtest2"));
        assertTrue(content.contains("actest3"));
        assertTrue(content.contains("adtest4"));

        DBString newEntry = new DBString("");
        newEntry.set("test2");
        splitList.addEntry(newEntry, "aftest5");
        splitList.flush();

        assertEquals(dir.readString("af/test5"), "test2");
    }

    public void testSplitDirList() throws Exception {
        IOStorageDir dir = new IOStorageDir(AsyncInterfaceUtil.fakeAsync(new MemoryIODatabase()), "");

        dir.putBytes("ab/test1/entry", "test1".getBytes());
        dir.putBytes("ab/test2/entry", "test1".getBytes());
        dir.putBytes("ac/test3/entry", "test1".getBytes());
        dir.putBytes("ad/test4/entry", "test1".getBytes());

        DBObjectList<DBString> splitList = new DBObjectList<>(new DBObjectList.SplitDirBackend() {
            @Override
            public IDBContainerEntry create(String entryName) {
                return new DBString("entry");
            }
        });
        splitList.setTo(dir);

        Collection<String> content = splitList.getDirContent().read().join();
        assertEquals(4, content.size());
        assertTrue(content.contains("abtest1"));
        assertTrue(content.contains("abtest2"));
        assertTrue(content.contains("actest3"));
        assertTrue(content.contains("adtest4"));

        assertEquals("test1", splitList.get("abtest1").read().join());

        DBString newEntry = new DBString("entry");
        newEntry.set("test2");
        splitList.addEntry(newEntry, "aftest5");
        splitList.flush();

        assertEquals(dir.readString("af/test5/entry"), "test2");
    }
}
