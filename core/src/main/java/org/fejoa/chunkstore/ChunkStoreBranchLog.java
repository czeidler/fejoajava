/*
 * Copyright 2016.
 * Distributed under the terms of the GPLv3 License.
 *
 * Authors:
 *      Clemens Zeidler <czei002@aucklanduni.ac.nz>
 */
package org.fejoa.chunkstore;

import java.io.*;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.concurrent.locks.Lock;


/**
 * There are two parts:
 * 1) log: contains a list of previous versions
 * 2) head: points to the latest or topmost commit
 */
public class ChunkStoreBranchLog {
    static public class Entry {
        int rev;
        HashValue id = Config.newBoxHash();
        String message = "";
        final List<HashValue> changes = new ArrayList<>();

        public Entry(int rev, HashValue id, String message) {
            this.rev = rev;
            this.id = id;
            this.message = message;
        }

        private Entry() {

        }

        public int getRev() {
            return rev;
        }

        public String getMessage() {
            return message;
        }

        public HashValue getEntryId() {
            return id;
        }

        static public Entry fromHeader(String header) {
            Entry entry = new Entry();
            if (header.equals(""))
                return entry;
            String[] parts = header.split(" ");
            if (parts.length != 3)
                return entry;

            entry.rev = Integer.parseInt(parts[0]);
            entry.id = HashValue.fromHex(parts[1]);
            entry.message = parts[2];
            return entry;
        }

        public String getHeader() {
            return "" + rev + " " + id.toHex() + " " + message;
        }

        static private Entry read(BufferedReader reader) throws IOException {
            String header = reader.readLine();
            if (header == null || header.length() == 0)
                return null;
            Entry entry = fromHeader(header);
            int nChanges;
            try {
                nChanges = Integer.parseInt(reader.readLine());
            } catch (Exception e) {
                // no change in this entry
                return entry;
            }
            for (int i = 0; i < nChanges; i++) {
                String line = reader.readLine();
                if (line == null)
                    throw new IOException("Missing change in log file");
                else
                    entry.changes.add(HashValue.fromHex(line));
            }
            return entry;
        }

        public void write(OutputStream outputStream, boolean writeChanges) throws IOException {
            outputStream.write((getHeader() + "\n").getBytes());
            if (!writeChanges)
                return;
            outputStream.write(("" + changes.size() + "\n").getBytes());
            for (HashValue change : changes)
                outputStream.write((change.toHex() + "\n").getBytes());
        }
    }

    final private String branch;
    private int latestRev = 1;
    final private File headFile;
    final private File logFile;
    final private Lock fileLock;
    private List<Entry> entries;
    private Entry headEntry;

    public ChunkStoreBranchLog(File logDir, String branch) throws IOException {
        this(logDir, branch, null);
    }

    public ChunkStoreBranchLog(File logDir, String branch, String remote) throws IOException {
        this.branch = branch;

        if (remote == null)
            logDir = getLocalDir(logDir);
        else
            logDir = new File(new File(logDir, "remote"), remote);


        File branchDir = new File(logDir, branch);
        this.fileLock = LockBucket.getInstance().getLock(branchDir.getAbsolutePath());

        headFile = new File(branchDir, "head");
        logFile = new File(branchDir, "log");

        readHead();
    }

    static private File getLocalDir(File logDir) {
        return new File(logDir, "local");
    }

    static public Collection<ChunkStoreBranchLog> listLocalBranches(File repoDir) {
        File dir = getLocalDir(repoDir);
        List<ChunkStoreBranchLog> branches = new ArrayList<>();
        String[] dirContent = dir.list();
        if (dirContent == null)
            return branches;
        for (String branch : dirContent) {
            File subEntry = new File(dir, branch);
            if (subEntry.isFile())
                continue;
            try {
                ChunkStoreBranchLog branchLog = new ChunkStoreBranchLog(repoDir, branch);
                if (branchLog.getHead() != null)
                    branches.add(branchLog);
            } catch (IOException e) {
                continue;
            }
        }
        return branches;
    }

    public String getBranch() {
        return branch;
    }

    private void lock() {
        fileLock.lock();
    }

    private void unlock() {
        fileLock.unlock();
    }

    public List<Entry> getEntries() throws IOException {
        if (entries == null)
            readLogs();

        return entries;
    }

    private List<Entry> readLogs() throws IOException {
        try {
            lock();

            if (entries == null)
                entries = new ArrayList<>();
            else
                entries.clear();
            // read log
            try {
                FileInputStream fileInputStream = new FileInputStream(logFile);
                BufferedReader reader = new BufferedReader(new InputStreamReader(fileInputStream));
                Entry entry;
                while ((entry = Entry.read(reader)) != null)
                    entries.add(entry);

                if (entries.size() > 0)
                    latestRev = entries.get(entries.size() - 1).rev + 1;
            } catch (FileNotFoundException e) {
            }
            return entries;
        } finally {
            unlock();
        }
    }

    private void readHead() throws IOException {
        try {
            lock();
            // read head
            try {
                FileInputStream fileInputStream = new FileInputStream(headFile);
                BufferedReader reader = new BufferedReader(new InputStreamReader(fileInputStream));
                headEntry = Entry.read(reader);
            } catch (FileNotFoundException e) {
            }
        } finally {
            unlock();
        }
    }

    private int nextRevId() {
        int currentRev = latestRev;
        latestRev++;
        return currentRev;
    }

    public Entry getHead() {
        return headEntry;
    }

    public void add(HashValue id, String message, List<HashValue> changes) throws IOException {
        Entry entry = new Entry(nextRevId(), id, message);
        entry.changes.addAll(changes);
        add(entry);
    }

    public void add(Entry entry) {
        try {
            lock();
            headEntry = entry;
            write(entry);
            if (entries != null)
                entries.add(entry);
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            unlock();
        }
    }

    private void write(Entry entry) throws IOException {
        // append to log
        if(!logFile.exists()) {
            logFile.getParentFile().mkdirs();
            logFile.createNewFile();
        }
        FileOutputStream logOutputStream = new FileOutputStream(logFile, true);
        try {
            entry.write(logOutputStream, true);
        } finally {
            logOutputStream.close();
        }

        // head
        if(!headFile.exists()) {
            headFile.getParentFile().mkdirs();
            headFile.createNewFile();
        }
        FileOutputStream headOutputStream = new FileOutputStream(headFile, false);
        try {
            entry.write(headOutputStream, false);
        } finally {
            headOutputStream.close();
        }
    }
}
