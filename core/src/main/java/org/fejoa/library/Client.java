/*
 * Copyright 2015.
 * Distributed under the terms of the GPLv3 License.
 *
 * Authors:
 *      Clemens Zeidler <czei002@aucklanduni.ac.nz>
 */
package org.fejoa.library;

import java8.util.concurrent.CompletableFuture;
import java8.util.function.Consumer;
import java8.util.function.Function;
import org.fejoa.library.command.*;
import org.fejoa.library.crypto.CryptoException;
import org.fejoa.library.crypto.CryptoHelper;
import org.fejoa.library.database.DatabaseDiff;
import org.fejoa.library.database.StorageDir;
import org.fejoa.library.remote.*;
import org.fejoa.library.support.Task;
import org.json.JSONException;
import org.json.JSONObject;

import javax.crypto.SecretKey;
import java.io.*;
import java.util.Collections;
import java.util.Scanner;
import java.util.concurrent.Executor;


public class Client {
    final static private String USER_SETTINGS_FILE = "user.settings";
    final private FejoaContext context;
    final private ConnectionManager connectionManager;
    final private MigrationManager migrationManager;
    private UserData userData;
    private ContactManager contactManager;
    private Task.IObserver<TaskUpdate, Void> syncObserver;
    private RemoteIndexSyncManager syncManager;
    private boolean isSyncing = false;
    private OutgoingQueueManager outgoingQueueManager;
    private IncomingCommandManager incomingCommandManager;

    final private StorageDir.IListener userDataStorageListener = new StorageDir.IListener() {
        @Override
        public void onTipChanged(DatabaseDiff diff) {
            if (isSyncing) {
                try {
                    startSyncing(syncObserver);
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }
    };

    private Client(File homeDir, Executor contextExecutor) {
        this.context = new FejoaContext(homeDir, contextExecutor);
        this.connectionManager = new ConnectionManager();
        this.migrationManager = new MigrationManager(this);
    }

    /**
     * Set branch location for all branches in the given context.
     */
    public void setBranchLocation(String context, Remote remote, AuthInfo authInfo)
            throws IOException, CryptoException {
        for (BranchInfo branchInfo : getUserData().getBranchList().getEntries(context, true))
            branchInfo.addLocation(remote.getId(), authInfo);
    }

    /**
     * Adds an existing remote and connect all user data branches to be synchronised to this remote
     */
    public CompletableFuture<Void> addGateWayRemote(final Remote remote, SecretKey loginKey)
            throws IOException, CryptoException {
        CompletableFuture<RemoteJob.Result> addRemoteJob = addRemote(remote, true, loginKey);
        final CompletableFuture<Void> resultFuture = new CompletableFuture<>();
        addRemoteJob.thenAcceptAsync(new Consumer<RemoteJob.Result>() {
            @Override
            public void accept(RemoteJob.Result result) {
                if (result.status != Errors.DONE) {
                    resultFuture.completeExceptionally(
                            new IOException("Fail to add remote " + remote.toAddress() + ": " + result.message));
                    return;
                }

                // connect branches
                try {
                    setBranchLocation(UserData.USER_DATA_CONTEXT, remote,
                            new AuthInfo.KeyStoreLogin(remote, getUserData().getKeyStore()));
                    resultFuture.complete(null);
                } catch (Exception e) {
                    resultFuture.completeExceptionally(e);
                }
            }
        }, getContextExecutor()).exceptionally(new Function<Throwable, Void>() {
            @Override
            public Void apply(Throwable throwable) {
                resultFuture.completeExceptionally(throwable);
                return null;
            }
        });
        return resultFuture;
    }

    static public Client init(File homeDir, Executor contextExecutor, String password)
            throws IOException, CryptoException {
        Client client = new Client(homeDir, contextExecutor);
        client.userData = UserData.create(client.context, password);
        client.setup();

        UserDataSettings userDataSettings = client.userData.getSettings();
        Writer writer = new BufferedWriter(new OutputStreamWriter(
                new FileOutputStream(Client.getUserDataSettingsFile(client.context))));
        writer.write(userDataSettings.toJson().toString());
        writer.flush();
        writer.close();

        return client;
    }

    static public Client open(File homeDir, Executor contextExecutor, String password) throws IOException, CryptoException, JSONException {
        Client client = new Client(homeDir, contextExecutor);
        client.userData = UserData.open(client.context, client.readUserDataSettings(), password);
        client.setup();
        return client;
    }

    private void setup() throws IOException, CryptoException {
        loadCommandManagers();
        userData.getStorageDir().addListener(userDataStorageListener);

        ContactRequestCommandHandler contactRequestCommandHandler
                = (ContactRequestCommandHandler)incomingCommandManager.getHandler(ContactRequestCommand.COMMAND_NAME);
        contactManager = new ContactManager(userData.getContactStore(), contactRequestCommandHandler);
    }

    public void close() {
        stopSyncing();
    }

    static public boolean exist(File homeDir) {
        return getUserDataSettingsFile(homeDir).exists();
    }

    public ContactManager getContactManager() {
        return contactManager;
    }

    static public File getUserDataSettingsFile(File homeDir) {
        return new File(homeDir, USER_SETTINGS_FILE);
    }

    static private File getUserDataSettingsFile(FejoaContext context) {
        return getUserDataSettingsFile(context.getHomeDir());
    }

    private UserDataSettings readUserDataSettings() throws FileNotFoundException, JSONException {
        return readUserDataSettings(getUserDataSettingsFile(context));
    }

    static public UserDataSettings readUserDataSettings(File dir) throws FileNotFoundException, JSONException {
        String content = new Scanner(dir).useDelimiter("\\Z").next();
        return new UserDataSettings(new JSONObject(content));
    }

    public void commit() throws IOException {
        userData.commit(true);
    }

    public FejoaContext getContext() {
        return context;
    }

    public Executor getContextExecutor() {
        return context.getContextExecutor();
    }

    public UserData getUserData() {
        return userData;
    }

    public ConnectionManager getConnectionManager() {
        return connectionManager;
    }

    public void contactRequest(String user, String server) throws Exception {
        ContactStore contactStore = userData.getContactStore();

        ContactPublic requestedContact = new ContactPublic(context, null);
        requestedContact.setId(CryptoHelper.sha1HashHex(user + "@" + server));
        requestedContact.getRemotes().add(new Remote(user, server), true);
        contactStore.getRequestedContacts().add(requestedContact);
        contactStore.commit();

        userData.getOutgoingCommandQueue().post(ContactRequestCommand.makeInitialRequest(
                userData.getMyself(),
                userData.getGateway()), user, server);
    }

    public SecretKey getBaseKey() throws IOException, CryptoException {
        UserDataSettings settings = userData.getSettings();
        return context.getBaseKey(settings.keyStoreSettings.keyStoreCrypto.userKeyParameters.baseKeyParameters);
    }

    public CompletableFuture<RegisterAccountJob.Result> registerAccount(Remote remote, String password)
            throws IOException, CryptoException {
        return connectionManager.submit(new RegisterAccountJob(userData.getContext(), remote.getUser(), password,
                        userData.getSettings()), remote, new AuthInfo.Plain());
    }

    public CompletableFuture<RegisterAccountJob.Result> registerAccount(Remote remote, SecretKey baseKey)
            throws IOException, CryptoException {
        return connectionManager.submit(new RegisterAccountJob(userData.getContext(), remote.getUser(), baseKey,
                userData.getSettings()), remote, new AuthInfo.Plain());
    }

    public CompletableFuture<RemoteJob.Result> addRemote(final Remote remote, final boolean setAsGateWay,
                                                         SecretKey loginKey) throws CryptoException {
        CompletableFuture<LoginJob.Result> login = connectionManager.submit(
                new LoginJob(getContext(), remote.getUser(), loginKey), remote, new AuthInfo.Plain());
        return addRemote(remote, setAsGateWay, login);
    }

    /**
     * Add a remote
     *
     * 1) Tests if the remote is accessible, i.e. if we can log in
     * 2) Adds the login key to the key store
     * 3) Adds the remote to the remote store
     */
    public CompletableFuture<RemoteJob.Result> addRemote(final Remote remote, final boolean setAsGateWay,
                                                PasswordProvider passwordProvider) throws CryptoException {
        CompletableFuture<LoginJob.Result> login = connectionManager.submit(
                new LoginJob(getContext(), remote.getUser(), passwordProvider), remote, new AuthInfo.Plain());
        return addRemote(remote, setAsGateWay, login);
    }

    private CompletableFuture<RemoteJob.Result> addRemote(final Remote remote, final boolean setAsGateWay,
                                                          CompletableFuture<LoginJob.Result> login)
            throws CryptoException {
        final CompletableFuture<RemoteJob.Result> resultFuture = new CompletableFuture();
        login.thenApplyAsync(new Function<LoginJob.Result, LoginJob.Result>() {
            @Override
            public LoginJob.Result apply(LoginJob.Result result) {
                if (result.status != Errors.DONE)
                    return result;
                try {
                    userData.getKeyStore().addLoginKey(remote, result.loginKey);
                    userData.getRemoteStore().add(remote);
                    if (setAsGateWay)
                        userData.setGateway(remote);
                    // restart syncing
                    if (isSyncing)
                        startSyncing(syncObserver);
                } catch (Exception e) {
                    resultFuture.completeExceptionally(e);
                }
                return result;
            }
        }, getContextExecutor()).thenAccept(new Consumer<LoginJob.Result>() {
            @Override
            public void accept(LoginJob.Result result) {
                resultFuture.complete(result);
            }
        });

        return resultFuture;
    }

    public void startSyncing(Task.IObserver<TaskUpdate, Void> observer) throws IOException, CryptoException {
        if (syncManager != null)
            stopSyncing();
        syncObserver = observer;
        syncManager = new RemoteIndexSyncManager(this, observer);
        isSyncing = true;
        syncManager.startWatching(userData.getRemoteStore().getEntries());
    }

    public void stopSyncing() {
        if (syncManager == null)
            return;
        syncManager.stopWatching();
        syncManager = null;
        isSyncing = false;
    }

    public RemoteIndexSyncManager getSyncManager() {
        return syncManager;
    }

    private void loadCommandManagers() throws IOException, CryptoException {
        outgoingQueueManager = new OutgoingQueueManager(getContextExecutor(), userData.getOutgoingCommandQueue(),
                connectionManager);
        incomingCommandManager = new IncomingCommandManager(this);
    }

    public void startCommandManagers(Task.IObserver<TaskUpdate, Void> outgoingCommandObserver)
            throws IOException, CryptoException {
        outgoingQueueManager.start(outgoingCommandObserver);
        incomingCommandManager.start();
    }

    public IncomingCommandManager getIncomingCommandManager() {
        return incomingCommandManager;
    }

    public void grantAccess(BranchInfo branchInfo, int rights, ContactPublic contact)
            throws IOException, JSONException, CryptoException {
        grantAccess(branchInfo.getBranch(), branchInfo.getStorageContext(), rights, contact);
    }

    public void grantAccess(String branch, String branchContext, int rights, ContactPublic contact)
            throws CryptoException, JSONException,
            IOException {
        // create and add new access token
        BranchAccessRight accessRight = new BranchAccessRight(BranchAccessRight.CONTACT_ACCESS);
        accessRight.addBranchAccess(branch, rights);
        AccessToken accessToken = AccessToken.create(context);
        accessToken.setAccessEntry(accessRight.toJson().toString());
        AccessStore accessStore = userData.getAccessStore();
        accessStore.addAccessToken(accessToken.toServerToken());

        // record with whom we share the branch
        BranchInfo branchInfo = userData.getBranchList().get(branch, branchContext);
        branchInfo.getContactAccessList().add(contact, accessToken);
        userData.commit();

        // send command to contact
        AccessCommand accessCommand = new AccessCommand(context, userData.getMyself(), contact, userData.getGateway(),
                branchInfo, branchContext, userData.getKeyData(branchInfo), accessToken);
        accessStore.commit();
        OutgoingCommandQueue queue = userData.getOutgoingCommandQueue();
        queue.post(accessCommand, contact.getRemotes().getDefault(), true);
    }

    public void postBranchUpdate(ContactPublic contact, String branch, String branchContext)
            throws IOException, CryptoException {
        BranchInfo branchInfo = userData.getBranchList().get(branch, branchContext);

        OutgoingCommandQueue queue = userData.getOutgoingCommandQueue();
        queue.post(new UpdateCommand(context, userData.getMyself(), contact, userData.getGateway(), branchInfo,
                branchContext), contact.getRemotes().getDefault(), true);
    }

    public void peekRemoteStatus(BranchInfo.Location location, Task.IObserver<Void, WatchJob.Result> observer)
            throws IOException, CryptoException {
        Remote remote = location.getRemote();
        AuthInfo authInfo = location.getAuthInfo(getUserData().getKeyStore());
        connectionManager.submit(new WatchJob(context,
                WatchJob.toWatchEntryList(context, Collections.singletonList(location)),true), remote, authInfo,
                observer);
    }

    public CompletableFuture<WatchJob.Result> peekRemoteStatus(BranchInfo.Location location)
            throws IOException, CryptoException {
        Remote remote = location.getRemote();
        AuthInfo authInfo = location.getAuthInfo(getUserData().getKeyStore());
        return connectionManager.submit(new WatchJob(context,
                        WatchJob.toWatchEntryList(context, Collections.singletonList(location)), true),
                remote, authInfo);
    }

    public void pullBranch(Remote remote, BranchInfo.Location location,
                           final Task.IObserver<Void, ChunkStorePullJob.Result> observer)
            throws IOException, CryptoException {
        AuthInfo authInfo = location.getAuthInfo(getUserData().getKeyStore());
        if (authInfo instanceof AuthInfo.Token) {
            AccessTokenContact token = ((AuthInfo.Token) authInfo).getToken();
            if ((token.getAccessRights().getEntries().get(0).getRights()
                    & BranchAccessRight.PULL) == 0)
                throw new IOException("missing rights!");
        }

        BranchInfo branchInfo = location.getBranchInfo();
        final StorageDir contactBranchDir = getContext().getStorage(branchInfo.getBranch(),
                branchInfo.getCryptoKey(), userData.getCommitSignature());

        SyncManager.pull(getConnectionManager(), contactBranchDir,
                remote, authInfo, observer);
    }

    public CompletableFuture<ChunkStorePullJob.Result> pullBranch(Remote remote, BranchInfo.Location location)
            throws IOException, CryptoException {
        AuthInfo authInfo = location.getAuthInfo(getUserData().getKeyStore());
        if (authInfo instanceof AuthInfo.Token) {
            AccessTokenContact token = ((AuthInfo.Token) authInfo).getToken();
            if ((token.getAccessRights().getEntries().get(0).getRights()
                    & BranchAccessRight.PULL) == 0)
                throw new IOException("missing rights!");
        }

        BranchInfo branchInfo = location.getBranchInfo();
        final StorageDir contactBranchDir = getContext().getStorage(branchInfo.getBranch(),
                branchInfo.getCryptoKey(), userData.getCommitSignature());

        return SyncManager.pull(getConnectionManager(), contactBranchDir,
                remote, authInfo);
    }

    public Task<Void, ChunkStorePullJob.Result> sync(StorageDir storageDir,
                                                     Remote remote, AuthInfo authInfo,
                                                     final Task.IObserver<TaskUpdate, String> observer) {
        return SyncManager.sync(getConnectionManager(), storageDir, remote, authInfo, observer);
    }

    public MigrationManager getMigrationManager() {
        return migrationManager;
    }
}
