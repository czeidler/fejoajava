/*
 * Copyright 2015.
 * Distributed under the terms of the GPLv3 License.
 *
 * Authors:
 *      Clemens Zeidler <czei002@aucklanduni.ac.nz>
 */
package org.fejoa.library.support;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.Executor;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.concurrent.locks.Condition;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;


/**
 * Executor for running jobs in a given order
 *
 * In contrast to the normal executor services the LooperExecutor can use a shared worker executor. For example,
 * multiple LooperExecutors can work their queue using a common thread pool while ensuring the local job order.
 */
public class LooperExecutor implements Executor {
    private boolean quit = false;
    final private List<Runnable> jobs;
    final private Executor executor;
    private AtomicBoolean isWorking = new AtomicBoolean(false);
    private Thread currentThread = null;

    /**
     *
     * @param executor the executor who does the real work
     */
    public LooperExecutor(int capacity, Executor executor) {
        // TODO use a blocking queue?
        jobs = new ArrayList<>(capacity);
        this.executor = executor;
    }

    public LooperExecutor(Executor executor) {
        this(20, executor);
    }

    private void work() {
        synchronized (this) {
            if (isWorking.get())
                return;
            if (jobs.size() == 0)
                return;
            isWorking.set(true);
            final Runnable job = jobs.remove(0);
            executor.execute(new Runnable() {
                @Override
                public void run() {
                    synchronized (LooperExecutor.this) {
                        currentThread = Thread.currentThread();
                    }
                    job.run();
                    synchronized (LooperExecutor.this) {
                        currentThread = null;
                        isWorking.set(false);
                        work();
                    }
                }
            });
        }
    }

    public long getCurrentThreadId() {
        synchronized (this) {
            if (currentThread == null)
                return -1;
            return currentThread.getId();
        }
    }

    public void quit(final boolean waitTillFinished) {
        synchronized (this) {
            if (quit)
                return;
            quit = true;
        }
        final Lock lock = new ReentrantLock();
        final Condition finished  = lock.newCondition();

        executeForced(new Runnable() {
            @Override
            public void run() {
                if (waitTillFinished) {
                    lock.lock();
                    try {
                        finished.signal();
                    } finally {
                        lock.unlock();
                    }
                }
            }
        }, false);
        if (waitTillFinished) {
            lock.lock();
            try {
                finished.await();
            } catch (InterruptedException e) {
                e.printStackTrace();
            } finally {
                lock.unlock();
            }
        }
    }

    @Override
    public void execute(Runnable runnable) {
        execute(runnable, false);
    }

    public boolean execute(Runnable runnable, boolean insertAtFront) {
        if (quit)
            return false;
        executeForced(runnable, insertAtFront);
        return true;
    }

    /**
     * Queues a job even after quit has been called (needed to send the final job)
     */
    private void executeForced(Runnable runnable, boolean insertAtFront) {
        synchronized (this) {
            if (insertAtFront)
                jobs.add(0, runnable);
            else
                jobs.add(runnable);
        }
        // start work
        work();
    }
}
