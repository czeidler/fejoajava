/*
 * Copyright 2017.
 * Distributed under the terms of the GPLv3 License.
 *
 * Authors:
 *      Clemens Zeidler <clemens.zeidler@auckland.ac.nz>
 */
package org.fejoa.library;

import org.fejoa.chunkstore.HashValue;
import org.fejoa.library.crypto.CryptoException;
import org.fejoa.library.database.*;

import java.io.IOException;
import java.util.Collection;


public class BranchIndex {
    public static class Entry extends DBObjectContainer {
        final public String branch;
        final private DBHashValueObject logTip = new DBHashValueObject(this, "");

        public Entry(String branch) {
            this.branch = branch;
        }

        public HashValue getLogTip() {
            return logTip.read().join();
        }

        public void setLogTip(HashValue logTip) {
            this.logTip.set(logTip);
        }
    }

    final private StorageDir storageDir;
    final private DBObjectConnector storage;
    final private DBObjectList<Entry> entryList = new DBObjectList<>(new DBObjectList.SplitFileBackend() {
        @Override
        public IDBContainerEntry create(String entryName) {
            return new Entry(entryName);
        }
    });

    public BranchIndex(StorageDir storageDir) {
        this.storageDir = storageDir;
        storage = new DBObjectConnector(entryList, storageDir);
    }

    public StorageDir getStorageDir() {
        return storageDir;
    }

    public String getIndexName() {
        return storageDir.getBranch();
    }

    public void update(String branch, HashValue logTip) throws IOException, CryptoException {
        Entry entry = entryList.get(branch);
        entry.setLogTip(logTip);
    }

    public Collection<Entry> getEntries() throws IOException, CryptoException {
        return entryList.getEntries();
    }

    public HashValue commit() throws IOException {
        return storage.commit();
    }
}
