/*
 * Copyright 2015.
 * Distributed under the terms of the GPLv3 License.
 *
 * Authors:
 *      Clemens Zeidler <czei002@aucklanduni.ac.nz>
 */
package org.fejoa.library.command

import java8.util.concurrent.CompletableFuture
import kotlinx.coroutines.experimental.CoroutineStart
import kotlinx.coroutines.experimental.asCoroutineDispatcher
import kotlinx.coroutines.experimental.async
import kotlinx.coroutines.experimental.delay
import org.fejoa.library.Remote
import org.fejoa.library.database.StorageDir
import org.fejoa.library.remote.*
import org.fejoa.library.support.Task
import org.fejoa.library.support.await

import java.util.ArrayList
import java.util.HashMap
import java.util.concurrent.Executor
import java.util.concurrent.TimeUnit


class OutgoingQueueManager(private val contextExecutor: Executor, queue: OutgoingCommandQueue, private val manager: ConnectionManager) {
    private val TASK_NAME = "Send Commands"
    private var running = false
    private val queues = ArrayList<OutgoingCommandQueue>()
    private var observer: Task.IObserver<TaskUpdate, Void>? = null
    // command hash to job callback
    private val runningSendJobs: MutableMap<String, CompletableFuture<RemoteJob.Result>> = HashMap()

    private val storageListener = StorageDir.IListener { sendCommands() }

    init {
        this.queues.add(queue)
    }

    fun start(observer: Task.IObserver<TaskUpdate, Void>) {
        this.observer = observer
        running = true

        queues.map { it.getStorageDir() }
                .forEach { it.addListener(storageListener) }
        sendCommands()
    }

    fun stop() {
        running = false

        for (queue in queues)
            queue.getStorageDir().removeListener(storageListener)

        runningSendJobs.forEach {
            it.value.cancel(false)
        }
    }

    private fun sendCommands() {
        for (queue in queues) {
            try {
                val commands = queue.commands
                for (i in commands.indices) {
                    val entry = commands[i]
                    async(contextExecutor.asCoroutineDispatcher(), CoroutineStart.UNDISPATCHED) {
                        sendWithRetry(queue, entry, observer, commands.size, i)
                    }
                }
            } catch (e: Exception) {
                e.printStackTrace()
            }

        }
    }

    private fun jobReturned(entry: OutgoingCommandQueue.Entry) {
        runningSendJobs.remove(entry.hash())
        if (runningSendJobs.size == 0)
            observer!!.onResult(null)
    }

    suspend private fun sendWithRetry(queue: OutgoingCommandQueue, entry: OutgoingCommandQueue.Entry,
                              observer: Task.IObserver<TaskUpdate, Void>?,
                              totalCommands: Int, currentCommand: Int) {
        if (runningSendJobs.containsKey(entry.hash()))
            return

        try {
            var retries: Int = 0
            while (running) {
                when (retries) {
                    0 -> { }
                    1 -> delay(1, TimeUnit.SECONDS)
                    2 -> delay(2, TimeUnit.SECONDS)
                    3 -> delay(30, TimeUnit.SECONDS)
                    4 -> delay(60, TimeUnit.SECONDS)
                    5 -> delay(10, TimeUnit.MINUTES)
                    else -> delay(30, TimeUnit.MINUTES)
                }

                if (!running)
                    break
                send(queue, entry, observer, totalCommands, currentCommand)
                retries++
            }
        } finally {
            jobReturned(entry)
        }
    }

    suspend private fun send(queue: OutgoingCommandQueue, entry: OutgoingCommandQueue.Entry,
                     observer: Task.IObserver<TaskUpdate, Void>?,
                     totalCommands: Int, currentCommand: Int): Boolean {
        // if run synchronously job may finish before we are able to put it into the map, thus add a preliminary null
        // item
        val job = manager.submit(SendCommandJob(entry.data, entry.user),
                Remote(entry.user, entry.server), AuthInfo.Plain())
        runningSendJobs[entry.hash()] = job

        try {
            val result = job.await()
            if (result.status == Errors.DONE) {
                observer?.onProgress(TaskUpdate(TASK_NAME, totalCommands, currentCommand + 1,
                        "command sent"))
                queue.removeCommand(entry)
                queue.commit()
                return true
            }
        } catch (e: Exception) {
            observer?.onProgress(TaskUpdate(TASK_NAME, 1, 1, "exception while sending"))
            observer?.onException(e)
            throw e
        }
        return false
    }
}
