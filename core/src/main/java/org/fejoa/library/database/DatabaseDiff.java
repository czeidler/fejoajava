/*
 * Copyright 2014.
 * Distributed under the terms of the GPLv3 License.
 *
 * Authors:
 *      Clemens Zeidler <czei002@aucklanduni.ac.nz>
 */
package org.fejoa.library.database;


import org.fejoa.chunkstore.HashValue;

import java.util.Collection;

public class DatabaseDiff {
    public enum ChangeType {
        ADDED,
        REMOVED,
        MODIFIED
    }

    static public class Entry {
        final public String path;
        final public ChangeType type;

        public Entry(String path, ChangeType type) {
            this.path = path;
            this.type = type;
        }
    }

    final public HashValue base;
    final public HashValue target;
    final public Collection<Entry> changes;

    public DatabaseDiff(HashValue base, HashValue target, Collection<Entry> changes) {
        this.base = base;
        this.target = target;
        this.changes = changes;
    }
}
