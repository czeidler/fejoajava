package org.fejoa.library.database;

import java8.util.concurrent.CompletableFuture;
import java8.util.concurrent.CompletionStage;
import java8.util.function.Function;
import org.fejoa.chunkstore.HashValue;


public class DBObjectConnector {
    final private IDBContainerEntry entry;
    final private StorageDir storageDir;

    private StorageDir.IListener listener = new StorageDir.IListener() {
        @Override
        public void onTipChanged(DatabaseDiff diff) {
            entry.invalidate();
        }
    };

    public DBObjectConnector(IDBContainerEntry entry, StorageDir storageDir) {
        this.entry = entry;
        this.storageDir = storageDir;
        entry.setTo(storageDir);
        storageDir.addListener(listener);
    }

    public CompletableFuture<HashValue> commitAsync() {
        return entry.flush().thenCompose(new Function<Void, CompletionStage<HashValue>>() {
            @Override
            public CompletionStage<HashValue> apply(Void aVoid) {
                return storageDir.commitAsync();
            }
        });
    }

    public HashValue commit() {
        return commitAsync().join();
    }
}
