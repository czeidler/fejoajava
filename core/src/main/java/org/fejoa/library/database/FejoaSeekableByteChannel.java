/*
 * Copyright 2016.
 * Distributed under the terms of the GPLv3 License.
 *
 * Authors:
 *      Clemens Zeidler <czei002@aucklanduni.ac.nz>
 */
package org.fejoa.library.database;


import org.fejoa.library.crypto.CryptoException;

import java.io.IOException;
import java.nio.ByteBuffer;
import java.nio.channels.SeekableByteChannel;

public class FejoaSeekableByteChannel implements SeekableByteChannel {
    final private ISyncRandomDataAccess randomDataAccess;

    public FejoaSeekableByteChannel(ISyncRandomDataAccess randomDataAccess) {
        this.randomDataAccess = randomDataAccess;
    }

    @Override
    public int read(ByteBuffer byteBuffer) throws IOException {
        byte[] buffer = new byte[byteBuffer.remaining()];
        int bytesRead;
        try {
            bytesRead = randomDataAccess.read(buffer);
        } catch (CryptoException e) {
            throw new IOException(e);
        }
        byteBuffer.put(buffer);
        return bytesRead;
    }

    @Override
    public int write(ByteBuffer byteBuffer) throws IOException {
        byte[] buffer = new byte[byteBuffer.remaining()];
        byteBuffer.get(buffer);
        randomDataAccess.write(buffer);
        return buffer.length;
    }

    @Override
    public long position() throws IOException {
        return randomDataAccess.position();
    }

    @Override
    public SeekableByteChannel position(long l) throws IOException {
        try {
            randomDataAccess.seek(l);
        } catch (CryptoException e) {
            throw new IOException(e);
        }
        return this;
    }


    @Override
    public long size() throws IOException {
        return randomDataAccess.length();
    }

    @Override
    public SeekableByteChannel truncate(long l) throws IOException {
        randomDataAccess.truncate(l);
        return this;
    }

    @Override
    public boolean isOpen() {
        return true;
    }

    @Override
    public void close() throws IOException {
        try {
            randomDataAccess.close();
        } catch (CryptoException e) {
            throw new IOException(e);
        }
    }
}
