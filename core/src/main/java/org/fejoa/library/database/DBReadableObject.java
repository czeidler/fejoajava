/*
 * Copyright 2016.
 * Distributed under the terms of the GPLv3 License.
 *
 * Authors:
 *      Clemens Zeidler <czei002@aucklanduni.ac.nz>
 */
package org.fejoa.library.database;

import java8.util.concurrent.CompletableFuture;
import java8.util.function.BiFunction;
import java8.util.function.Function;


public abstract class DBReadableObject<T> implements IDBContainerEntry {
    protected IOStorageDir dir;
    final protected String path;
    protected T cache;

    public DBReadableObject(String path) {
        this.path = path;
    }

    public DBReadableObject(IOStorageDir dir, String path) {
        this(path);

        setTo(dir);
    }

    public DBReadableObject(DBObjectContainer parent, String path) {
        this(path);
        parent.add(this);
    }

    abstract protected CompletableFuture<T> readFromDB(IOStorageDir dir, String path);

    @Override
    public void setTo(IOStorageDir dir) {
        this.dir = dir;
    }

    protected void setCache(T value) {
        synchronized (this) {
            this.cache = value;
        }
    }

    /**
     * Reads the object value
     *
     * @return the completable future contains null if the value couldn't be read
     */
    public CompletableFuture<T> read() {
        synchronized (this) {
            if (dir == null || cache != null)
                return CompletableFuture.completedFuture(cache);

            return readFromDB(dir, path).handle(new BiFunction<T, Throwable, T>() {
                @Override
                public T apply(T t, Throwable throwable) {
                    if (t != null)
                        return t;
                    return null;
                }
            }).thenApply(new Function<T, T>() {
                @Override
                public T apply(T value) {
                    synchronized (DBReadableObject.this) {
                        // check if the cache has been set in the meantime; don't overwrite in this case
                        if (cache != null)
                            return cache;
                        setCache(value);
                        return value;
                    }
                }
            });
        }
    }

    public CompletableFuture<Boolean> exists() {
        return read().thenApply(new Function<T, Boolean>() {
            @Override
            public Boolean apply(T t) {
                return t != null;
            }
        });
    }

    @Override
    public CompletableFuture<Void> flush() {
        return CompletableFuture.completedFuture(null);
    }

    @Override
    public void invalidate() {
        setCache(null);
    }
}
