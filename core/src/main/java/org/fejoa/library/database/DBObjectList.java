/*
 * Copyright 2016.
 * Distributed under the terms of the GPLv3 License.
 *
 * Authors:
 *      Clemens Zeidler <czei002@aucklanduni.ac.nz>
 */
package org.fejoa.library.database;

import java8.util.concurrent.CompletableFuture;
import java8.util.function.Function;
import java8.util.function.Predicate;
import java8.util.stream.Collectors;
import java8.util.stream.Stream;
import java8.util.stream.StreamSupport;
import kotlin.Pair;
import org.fejoa.library.support.StorageLib;

import java.lang.ref.WeakReference;
import java.util.*;


abstract class DBSplitReaderBase extends DBReadableObject<Collection<String>> {
    public DBSplitReaderBase() {
        super("");
    }

    abstract protected CompletableFuture<Collection<String>> readSecond(IOStorageDir dir, String path);

    @Override
    protected CompletableFuture<Collection<String>> readFromDB(final IOStorageDir dir, final String path) {
        return dir.listDirectoriesAsync(path).thenApply(new Function<Collection<String>, Collection<String>>() {
            @Override
            public Collection<String> apply(Collection<String> dirs) {
                return StreamSupport.stream(dirs).filter(new Predicate<String>() {
                    @Override
                    public boolean test(String s) {
                        return s.length() == 2;
                    }
                }).map(new Function<String, Pair<String, CompletableFuture<Collection<String>>>>() {
                    @Override
                    public Pair<String, CompletableFuture<Collection<String>>> apply(String sub) {
                        return new Pair(sub, readSecond(dir, StorageLib.appendDir(path, sub)));
                    }
                }).flatMap(new Function<Pair<String, CompletableFuture<Collection<String>>>, Stream<String>>() {
                    @Override
                    public Stream<String> apply(final Pair<String, CompletableFuture<Collection<String>>> result) {
                        return StreamSupport.stream(result.getSecond().join()).map(new Function<String, String>() {
                            @Override
                            public String apply(String secondPart) {
                                return result.getFirst() + secondPart;
                            }
                        });
                    }
                }).collect(Collectors.<String>toList());
            }
        });
    }
}

class DBSplitFileReader extends DBSplitReaderBase {
    public DBSplitFileReader() {
        super();
    }

    @Override
    protected CompletableFuture<Collection<String>> readSecond(IOStorageDir dir, String path) {
        return dir.listFilesAsync(path);
    }
}

class DBSplitDirReader extends DBSplitReaderBase {
    public DBSplitDirReader() {
        super();
    }

    @Override
    protected CompletableFuture<Collection<String>> readSecond(IOStorageDir dir, String path) {
        return dir.listDirectoriesAsync(path);
    }
}

public class DBObjectList<T extends IDBContainerEntry> extends DBObjectContainer {
    static public abstract class Backend<T extends IDBContainerEntry> {
        abstract public T create(String entryName);
        abstract public DBReadableObject<Collection<String>> getContentListReader();
        public String idToPath(String id) {
            return id;
        }
    }

    /**
     * Backend for entries that are stored in subdirectories
     *
     * For example: dir/entry
     */
    static public abstract class DirBackend<T extends IDBContainerEntry> extends Backend<T> {
        @Override
        public DBReadableObject<Collection<String>> getContentListReader() {
            return new DBDirReader();
        }
    }

    /**
     * Backend for entries that are stored in files
     */
    static public abstract class FileBackend<T extends IDBContainerEntry> extends Backend<T> {
        @Override
        public DBReadableObject<Collection<String>> getContentListReader() {
            return new DBFileReader();
        }
    }

    static protected String itToSplitPath(String id) {
        String dir = id.substring(0, 2);
        String rest = id.substring(2);
        return dir + "/" + rest;
    }

    /**
     * Backend for entries that are stored in two subdirectories where the first sub directory has length 2
     *
     * For example: aa/b1233/entry
     */
    static public abstract class SplitDirBackend<T extends IDBContainerEntry> extends Backend<T> {
        @Override
        public DBReadableObject<Collection<String>> getContentListReader() {
            return new DBSplitDirReader();
        }

        @Override
        public String idToPath(String id) {
            return itToSplitPath(id);
        }
    }

    /**
     * Backend for file entries that are stored in a subdirectory where the the sub directory has length 2
     *
     * For example: en/try or aa/entryid
     */
    static public abstract class SplitFileBackend<T extends IDBContainerEntry> extends Backend<T> {
        @Override
        public DBReadableObject<Collection<String>> getContentListReader() {
            return new DBSplitFileReader();
        }

        @Override
        public String idToPath(String id) {
            return itToSplitPath(id);
        }
    }

    final private Backend<T> valueCreator;
    final private DBReadableObject<Collection<String>> dirContent;
    final private Map<String, WeakReference<T>> loadedEntries = new HashMap<>();

    public DBObjectList(Backend backend) {
        this(null, backend);
    }

    public DBObjectList(DBObjectContainer parent, Backend backend) {
        super(parent);
        this.valueCreator = backend;
        this.dirContent = backend.getContentListReader();
        add(dirContent, "");
    }

    public DBReadableObject<Collection<String>> getDirContent() {
        return dirContent;
    }

    public Collection<T> getEntries() {
        Collection<String> content = dirContent.read().join();
        List<T> list = new ArrayList<>();
        for (String id : content)
            list.add(get(id));
        return list;
    }

    public T get(String id) {
        WeakReference<T> weakReference = loadedEntries.get(id);
        if (weakReference != null) {
            T value = weakReference.get();
            if (value != null)
                return value;
        }

        T value = valueCreator.create(id);
        addEntry(value, id);
        return value;
    }

    public void addEntry(T entry, String id) {
        add(entry, valueCreator.idToPath(id));
        loadedEntries.put(id, new WeakReference<>(entry));
    }

    @Override
    public void invalidate() {
        super.invalidate();

        List<String> invalid = new ArrayList<>();
        for (Map.Entry<String, WeakReference<T>> entry : loadedEntries.entrySet()) {
            if (entry.getValue().get() == null)
                invalid.add(entry.getKey());
        }
        for (String key : invalid)
            loadedEntries.remove(key);
    }
}
