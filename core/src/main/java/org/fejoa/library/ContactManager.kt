package org.fejoa.library

import org.fejoa.library.command.ContactRequestCommandHandler


class ContactManager(val contactStore: ContactStore, val handler: ContactRequestCommandHandler) {
    fun getContacts(): Collection<ContactPublic> {
        return contactStore.contactList.entries
    }

    fun getRequestedContacts(): Collection<ContactPublic> {
        return contactStore.requestedContacts.entries
    }

    fun getContactRequests(): Collection<ContactRequestCommandHandler.ContactRequest> {
        return handler.contactRequests
    }

    fun setContactRequestListener(listener: ContactRequestCommandHandler.IListener) {
        handler.listener = listener
    }
}
