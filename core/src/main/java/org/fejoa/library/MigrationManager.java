/*
 * Copyright 2015.
 * Distributed under the terms of the GPLv3 License.
 *
 * Authors:
 *      Clemens Zeidler <czei002@aucklanduni.ac.nz>
 */
package org.fejoa.library;

import java8.util.concurrent.CompletableFuture;
import java8.util.function.Consumer;
import org.fejoa.library.command.MigrationCommand;
import org.fejoa.library.command.OutgoingCommandQueue;
import org.fejoa.library.crypto.CryptoException;
import org.fejoa.library.support.Task;
import org.fejoa.library.remote.*;

import javax.crypto.SecretKey;
import java.util.ArrayList;
import java.util.List;


public class MigrationManager {
    final private Client client;

    public MigrationManager(Client client) {
        this.client = client;
    }

    public void migrate(final Remote newRemote, final SecretKey newRemoteLoginKey,
                        final Task.IObserver<Void, RemoteJob.Result> observer)
            throws Exception {
        //TODO this method has to be reworked properly and be made async.
        // 1) lock local account
        // 2) register new remote (client.addRemote)
        // 3) Clean up the added remote when migration failed?

        // create access token for the new server
        final AccessToken accessToken = AccessToken.create(client.getContext());
        BranchAccessRight accessRight = new BranchAccessRight(BranchAccessRight.MIGRATION_ACCESS);
        final List<BranchInfo> branchesToCopy = new ArrayList<>();
        for (BranchInfo branchInfo : client.getUserData().getBranchList().getEntries()) {
            accessRight.addBranchAccess(branchInfo.getBranch(), BranchAccessRight.PULL_CHUNK_STORE);
            branchesToCopy.add(branchInfo);
        }
        if (branchesToCopy.size() == 0)
            throw new Exception("no branches to migrate");
        accessToken.setAccessEntry(accessRight.toJson().toString());
        final AccessTokenContact accessTokenContact = accessToken.toContactToken();

        final Remote currentRemote = client.getUserData().getGateway();
        client.getConnectionManager().submit(new StartMigrationJob(currentRemote.getUser(),
                        accessToken.toServerToken()),
                currentRemote,
                new AuthInfo.KeyStoreLogin(currentRemote, client.getUserData().getKeyStore()),
                new Task.IObserver<Void, RemoteJob.Result>() {
                    @Override
                    public void onProgress(Void update) {

                    }

                    @Override
                    public void onResult(RemoteJob.Result result) {
                        if (result.status != Errors.DONE) {
                            observer.onException(new Exception(result.message));
                            return;
                        }
                        copyBranches(currentRemote, branchesToCopy, newRemote, accessTokenContact,
                                newRemoteLoginKey, new Task.IObserver<Void, RemoteJob.Result>() {
                            @Override
                            public void onProgress(Void aVoid) {
                                observer.onProgress(aVoid);
                            }

                            @Override
                            public void onResult(RemoteJob.Result result) {
                                // add new remote and commit
                                try {
                                    client.addRemote(newRemote, false, newRemoteLoginKey)
                                            .thenAcceptAsync(new Consumer<RemoteJob.Result>() {
                                        @Override
                                        public void accept(RemoteJob.Result result) {
                                            if (result.status != Errors.DONE) {
                                                observer.onException(new Exception("Failed to add new remote "
                                                        + newRemote.toAddress() + ": " + result.message));
                                                return;
                                            }
                                            try {
                                                client.commit();
                                                observer.onResult(result);
                                                // (the latest committed changes are pushed to the new CSP)
                                            } catch (Exception e) {
                                                observer.onException(e);
                                            }
                                        }
                                    }, client.getContextExecutor());
                                } catch (CryptoException e) {
                                    observer.onException(e);
                                }
                            }

                            @Override
                            public void onException(Exception exception) {
                                observer.onException(exception);
                            }
                        });


                    }

                    @Override
                    public void onException(Exception exception) {
                        observer.onException(exception);
                    }
                });
    }

    private void copyBranches(final Remote currentRemote, final List<BranchInfo> branchesToCopy,
                              final Remote updatedRemote,
                              final AccessTokenContact accessTokenContact,
                              final SecretKey loginKey,
                              final Task.IObserver<Void, RemoteJob.Result> observer) {
        client.getConnectionManager().submit(
                new RemotePullJob(updatedRemote.getUser(), accessTokenContact, branchesToCopy.get(0).getBranch(),
                        currentRemote.getUser(),
                        currentRemote.getServer()),
                updatedRemote,
                new AuthInfo.LoginKey(client.getContext(), loginKey),
                new Task.IObserver<Void, RemoteJob.Result>() {
                    @Override
                    public void onProgress(Void update) {

                    }

                    @Override
                    public void onResult(RemoteJob.Result result) {
                        if (result.status != Errors.DONE) {
                            observer.onException(new Exception(result.message));
                            return;
                        }
                        branchesToCopy.remove(0);
                        if (branchesToCopy.size() > 0) {
                            copyBranches(currentRemote, branchesToCopy, updatedRemote, accessTokenContact,
                                    loginKey, observer);
                        } else {
                            // update own remote
                            try {
                                UserData userData = client.getUserData();
                                userData.getRemoteStore().add(updatedRemote);
                                userData.commit();
                            } catch (Exception e) {
                                onException(e);
                            }
                            notifyContacts(updatedRemote, observer);
                        }
                    }

                    @Override
                    public void onException(Exception exception) {
                        observer.onException(exception);
                    }
                });
    }

    private void notifyContacts(final Remote updatedRemote,
                                final Task.IObserver<Void, RemoteJob.Result> observer) {
        ContactPrivate myself = client.getUserData().getMyself();
        for (ContactPublic contactPublic : client.getUserData().getContactStore().getContactList().getEntries()) {
            try {
                OutgoingCommandQueue queue = client.getUserData().getOutgoingCommandQueue();
                queue.post(new MigrationCommand(client.getContext(), updatedRemote, myself, contactPublic),
                        contactPublic.getRemotes().getDefault(), true);
            } catch (Exception e) {
                // report error
                //observer.onException(e);
            }
        }
        observer.onResult(new RemoteJob.Result(Errors.DONE, "migration done"));
    }
}
