package org.fejoa.library.remote;

import org.fejoa.library.BranchInfo;
import org.fejoa.library.UserData;
import org.fejoa.library.database.DatabaseDiff;
import org.fejoa.library.database.StorageDir;

import java.util.Collection;
import java.util.HashMap;
import java.util.Map;


public class LocalBranchMonitor {
    public interface IListener {
        void onTipChanged(BranchInfo branchInfo, DatabaseDiff diff);
    }

    static private class Entry {
        final public StorageDir storageDir;
        final public StorageDir.IListener listener;

        public Entry(StorageDir storageDir, StorageDir.IListener listener) {
            this.storageDir = storageDir;
            this.listener = listener;
        }
    }

    final private UserData userData;
    final private Map<BranchInfo.Location, Entry> branchInfoMap = new HashMap<>();
    private IListener currentListener;

    public LocalBranchMonitor(UserData userData) {
        this.userData = userData;
    }

    public void startWatching(Collection<BranchInfo.Location> branchInfoList, IListener listener) {
        stopWatching();
        currentListener = listener;
        for (BranchInfo.Location location : branchInfoList) {
            try {
                BranchInfo branchInfo = location.getBranchInfo();
                StorageDir storageDir = userData.getStorageDir(branchInfo);
                StorageDir.IListener storageListener = createListener(branchInfo);
                storageDir.addListener(storageListener);
                branchInfoMap.put(location, new Entry(storageDir, storageListener));
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    private StorageDir.IListener createListener(final BranchInfo branchInfo) {
        return new StorageDir.IListener() {
            @Override
            public void onTipChanged(DatabaseDiff diff) {
                currentListener.onTipChanged(branchInfo, diff);
            }
        };
    }

    public void stopWatching() {
        if (branchInfoMap.size() == 0)
            return;

        for (Map.Entry<BranchInfo.Location, Entry> it : branchInfoMap.entrySet()) {
            Entry entry = it.getValue();
            entry.storageDir.removeListener(entry.listener);
        }

        branchInfoMap.clear();
    }
}
