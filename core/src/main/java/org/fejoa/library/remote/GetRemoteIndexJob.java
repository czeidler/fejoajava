package org.fejoa.library.remote;

import org.fejoa.library.Constants;
import org.json.JSONObject;

import java.io.InputStream;

public class GetRemoteIndexJob extends SimpleJsonRemoteJob<GetRemoteIndexJob.Result> {
    public static class Result extends RemoteJob.Result {
        final public String branch;

        public Result(int status, String message, String branch) {
            super(status, message);

            this.branch = branch;
        }
    }

    final static public String METHOD = "getRemoteIndex";
    final private String serverUser;

    public GetRemoteIndexJob(String serverUser) {
        super(false);

        this.serverUser = serverUser;
    }

    @Override
    public String getJsonHeader(JsonRPC jsonRPC) throws Exception {
        return jsonRPC.call(METHOD, new JsonRPC.Argument(Constants.SERVER_USER_KEY, serverUser));
    }

    @Override
    protected Result handleJson(JSONObject returnValue, InputStream binaryData) {
        if (!returnValue.has(Constants.BRANCH_KEY))
            return new Result(Errors.ERROR, "no remote branch index", "");

        String branchName = returnValue.getString(Constants.BRANCH_KEY);
        return new Result(Errors.OK, "branch index name received", branchName);
    }
}
