package org.fejoa.library.remote

import java8.util.concurrent.CompletableFuture
import kotlinx.coroutines.experimental.*
import org.fejoa.chunkstore.Config
import org.fejoa.chunkstore.HashValue
import org.fejoa.chunkstore.Repository
import org.fejoa.library.*
import org.fejoa.library.database.StorageDir
import org.fejoa.library.support.Task
import org.fejoa.library.support.await
import java.io.File
import java.util.*
import kotlin.collections.ArrayList
import kotlin.collections.HashMap


/**
 * Sync local branches with the remotes
 *
 * To do so it watches and syncs the remote branch index and triggers the synchronization process.
 */
class RemoteIndexSyncManager(val client: Client, private val observer: Task.IObserver<TaskUpdate, Void>) {
    private val remoteWorkers: HashMap<String, Worker> = HashMap()

    internal class Worker(val client: Client, val remote: Remote,
                          private val observer: Task.IObserver<TaskUpdate, Void>) {
        private var isWorking: Boolean = false
        private var remoteIndexBranch: String? = null
        private var getIndexJob: CompletableFuture<GetRemoteIndexJob.Result>? = null
        private var watchJob: CompletableFuture<WatchJob.Result>? = null
        private var remoteSyncManager: RemoteSyncManager? = null
        private var contactNotifier: ContactBranchUpdateNotifier? = null

        private fun getRemoteBranchIndexStorageDir(branchName: String): StorageDir {
            val fejoaContext = client.context
            val baseDir = File(fejoaContext.homeDir, "branchIndex/remote")
            val indexDir = File(baseDir, remote.id)
            return fejoaContext.getPlainStorage(indexDir, branchName)
        }

        suspend fun work() {
            while (isWorking) {
                val branch = getRemoteIndexBranch() ?: break
                val indexStorageDir = getRemoteBranchIndexStorageDir(branch)

                // initialize contact notifier
                if (contactNotifier == null) {
                    val notificationStatus = client.userData.notificationStatus
                    contactNotifier = ContactBranchUpdateNotifier(client, BranchIndex(indexStorageDir), remote,
                            notificationStatus)
                }

                var logTip = (indexStorageDir.database as Repository).branchLog.head?.entryId
                if (logTip == null)
                    logTip = Config.newBoxHash()
                val authInfo = AuthInfo.KeyStoreLogin(remote, client.userData.keyStore)
                watchJob = client.connectionManager.submit(WatchJob(client.context,
                        Collections.singletonList(WatchJob.WatchEntry(remote.user, branch, logTip))),
                        remote, authInfo)
                val watchResult = watchJob!!.await()
                watchJob = null

                if (watchResult.updated == null || watchResult.updated.size == 0) {
                    observer.onProgress(TaskUpdate("Watching Remote Index", -1, -1,
                            "timeout (" + indexStorageDir.branch + ")"))
                    continue
                }

                val pullStatus = AsyncSyncer.pull(client.connectionManager, indexStorageDir, remote, authInfo)
                observer.onProgress(TaskUpdate("Pull Remote Index", -1, -1,
                        pullStatus.message + " (" + indexStorageDir.branch + ")"))

                if (remoteSyncManager == null)
                    remoteSyncManager = RemoteSyncManager(client, remote, indexStorageDir, observer)
                remoteSyncManager?.sync()
            }
        }

        suspend private fun getRemoteIndexBranch(): String? {
            if (remoteIndexBranch != null)
                return remoteIndexBranch

            val authInfo = AuthInfo.KeyStoreLogin(remote, client.userData.keyStore)
            getIndexJob = client.connectionManager.submit(GetRemoteIndexJob(remote.user),
                    remote, authInfo)
            val result = getIndexJob!!.await()
            getIndexJob = null
            if (result.status != Errors.OK) {
                observer.onException(Exception("Failed to get remote index: ${result.message}$"))
                stop()
                return null
            }
            remoteIndexBranch = result.branch
            return remoteIndexBranch
        }

        fun start() {
            if (isWorking)
                return
            isWorking = true
            launch(client.context.contextExecutor.asCoroutineDispatcher()) {
                try {
                    work()
                } catch (exception: Exception) {
                    if (exception !is CancellationException)
                        observer.onException(exception)
                }
            }
        }

        fun stop() {
            isWorking = false

            getIndexJob?.cancel(true)
            getIndexJob = null

            watchJob?.cancel(true)
            watchJob = null

            remoteSyncManager?.stop()
            remoteSyncManager = null
        }
    }

    fun startWatching(remotes: Collection<Remote>) {
        stopWatching()

        remotes.forEach { remote ->
            val worker = Worker(client, remote, observer)
            remoteWorkers[remote.id] = worker
            worker.start()
        }
    }

    fun stopWatching() {
        remoteWorkers.forEach { _, second -> second.stop() }
        remoteWorkers.clear()
    }
}


/**
 * Syncs local branches with a remote.
 *
 * Uses the remote index to determine which branches needs to be updated and syncs them if needed.
 * Furthermore, it monitors local branch changes and syncs them.
 */
class RemoteSyncManager(val client: Client, val remote: Remote, private val remoteBranchStorage: StorageDir,
                        val observer: Task.IObserver<TaskUpdate, Void>) {
    val userData: UserData = client.userData
    val fejoaContext: FejoaContext = userData.context

    private val localBranchMonitor: LocalBranchMonitor = LocalBranchMonitor(userData)
    private val localBranchTips: HashMap<BranchInfo, HashValue> = HashMap()
    private var syncJobRunning = false
    private var syncJob: Job? = null
    private val failedSyncs: ArrayList<FailedSync> = ArrayList()

    private inner class FailedSync(private val location: BranchInfo.Location) {
        var delayTime = 500L
        var job: Job? = null

        fun startRetrying() {
            val that = this
            failedSyncs.add(that)
            job = launch(fejoaContext.contextExecutor.asCoroutineDispatcher()) {
                var notSynced = true
                while (notSynced) {
                    delay(delayTime)
                    delayTime *= 2
                    val dirtyStorageDir = userData.getStorageDir(location.branchInfo)
                    try {
                        val result = AsyncSyncer.sync(client.connectionManager, dirtyStorageDir, remote,
                                location.getAuthInfo(client.userData.keyStore))
                        if (result.status == Errors.DONE)
                            notSynced = false

                        observer.onProgress(TaskUpdate("Sync2", -1, -1, result.message
                            + " (" + location.branchInfo.description + ")"))
                    } catch (e: Exception) {
                        observer.onException(e)
                    }
                }
                failedSyncs.remove(that)
            }
        }
    }

    fun stop() {
        localBranchMonitor.stopWatching()
    }

    var syncRequested = false
    fun sync() {
        syncRequested = true
        if (syncJobRunning)
            return

        syncJobRunning = true
        syncJob = launch(fejoaContext.contextExecutor.asCoroutineDispatcher()) {
            while (syncRequested) {
                syncRequested = false
                syncAsync()
            }
            syncJobRunning = false
        }
    }

    suspend fun syncAsync() {
        // TODO cache and somehow keep branch infos up to date
        val localTips = reloadLocalBranchIndex()
        val dirtyBranches = ArrayList<BranchInfo>()
        val entries = BranchIndex(remoteBranchStorage).entries
        localTips.forEach { branchInfo, hashValue ->
            if (entries.indexOfFirst { it.branch == branchInfo.branch } >= 0) {
                entries.filter { it.branch == branchInfo.branch && it.logTip != hashValue }
                        .map { dirtyBranches.add(branchInfo) }
            } else
                dirtyBranches.add(branchInfo)
        }

        syncDirtyBranches(dirtyBranches)
    }

    suspend private fun syncDirtyBranches(dirtyBranches: ArrayList<BranchInfo>) {
        if (dirtyBranches.size == 0)
            return

        var counter = 0
        for (branch in dirtyBranches) {
            for (location in branch.locationEntries) {
                if (location.remoteId == remote.id) {
                    val dirtyStorageDir = userData.getStorageDir(branch)
                    counter++
                    try {
                        val result = AsyncSyncer.sync(client.connectionManager, dirtyStorageDir, remote,
                                location.getAuthInfo(client.userData.keyStore))
                        if (result.status != Errors.DONE)
                            FailedSync(location).startRetrying()

                        observer.onProgress(TaskUpdate("Sync", dirtyBranches.size, counter, result.message
                            + " (" + branch.description + ")"))
                    } catch (exception: Exception) {
                        observer.onException(exception)
                    }

                }
            }
        }
        // don't sync automatically cause the just updated remote branch index needs to be retrieved first
        //sync()
    }


    private fun reloadLocalBranchIndex(): HashMap<BranchInfo, HashValue> {
        localBranchTips.clear()
        val entries = userData.branchList.getEntries(true)
        for (entry in entries) {
            val containsRemote = entry.locationEntries.any {
                val remote = it.remote
                if (remote == null)
                    false
                else
                    remote.id == this.remote.id
            }
            if (containsRemote) {
                val tip = fejoaContext.getStorageLogTip(userData.getStorageDir(entry))
                update(entry, tip)
            }
        }

        val locationList = ArrayList<BranchInfo.Location>()
        for (entry in entries)
            locationList.addAll(entry.locationEntries)

        localBranchMonitor.startWatching(locationList, { storageDir, diff ->
            //update(storageDir)
            sync()
        })

        return localBranchTips
    }

    private fun update(branchInfo: BranchInfo, hashValue: HashValue) {
        localBranchTips[branchInfo] = hashValue
    }
}
