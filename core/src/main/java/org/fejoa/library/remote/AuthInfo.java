/*
 * Copyright 2016.
 * Distributed under the terms of the GPLv3 License.
 *
 * Authors:
 *      Clemens Zeidler <czei002@aucklanduni.ac.nz>
 */
package org.fejoa.library.remote;

import org.fejoa.library.AccessTokenContact;
import org.fejoa.library.FejoaContext;
import org.fejoa.library.KeyStore;
import org.fejoa.library.Remote;
import org.fejoa.library.crypto.CryptoException;
import org.fejoa.library.database.IOStorageDir;

import javax.crypto.SecretKey;
import java.io.IOException;


abstract public class AuthInfo {
    static public class Plain extends AuthInfo {
        public Plain() {
            super(PLAIN);
        }

        @Override
        String getId() {
            return PLAIN;
        }
    }

    /**
     * Use the provided loginKey for login
     */
    static public class LoginKey extends AuthInfo {
        final public FejoaContext context;
        final public SecretKey loginKey;

        public LoginKey(FejoaContext context, SecretKey loginKey) {
            super(LOGIN_KEY);
            this.context = context;
            this.loginKey = loginKey;
        }

        @Override
        String getId() {
            return LOGIN_KEY;
        }
    }

    /**
     * Use a login key for the key store
     */
    static public class KeyStoreLogin extends AuthInfo {
        final public Remote remote;
        final public KeyStore keyStore;

        public KeyStoreLogin(Remote remote, KeyStore keyStore) {
            super(KEY_STORE_LOGIN_KEY);

            this.remote = remote;
            this.keyStore = keyStore;
        }

        @Override
        String getId() {
            return KEY_STORE_LOGIN_KEY;
        }
    }

    static public class Token extends AuthInfo {
        final public AccessTokenContact token;

        public Token(AccessTokenContact token) {
            super(TOKEN);
            this.token = token;
        }

        @Override
        public void write(IOStorageDir dir) throws IOException {
            super.write(dir);

            dir.writeString(AUTH_RAW_TOKEN_KEY, token.toJson().toString());
        }

        @Override
        String getId() {
            return token.getId();
        }

        public AccessTokenContact getToken() {
            return token;
        }
    }

    final static public String AUTH_TYPE_KEY = "authType";
    final static public String AUTH_RAW_TOKEN_KEY = "token";

    static public AuthInfo read(IOStorageDir storageDir, Remote remote, KeyStore keyStore)
            throws IOException, CryptoException {
        String type = storageDir.readString(AUTH_TYPE_KEY);
        switch (type) {
            case PLAIN:
                return new Plain();
            case LOGIN_KEY:
                throw new RuntimeException("Password should not stored directly");
            case KEY_STORE_LOGIN_KEY:
                return new KeyStoreLogin(remote, keyStore);
            case TOKEN:
                String rawToken = storageDir.readString(AUTH_RAW_TOKEN_KEY);
                AccessTokenContact tokenContact = new AccessTokenContact(keyStore.getContext(), rawToken);
                return new Token(tokenContact);
            default:
                return null;
        }
    }

    final static public String PLAIN = "plain";
    final static public String LOGIN_KEY = "loginKey";
    final static public String KEY_STORE_LOGIN_KEY = "keyStoreLoginKey";
    final static public String TOKEN = "token";

    final public String authType;

    protected AuthInfo(String type) {
        this.authType = type;
    }

    public void write(IOStorageDir dir) throws IOException {
        dir.writeString(AUTH_TYPE_KEY, authType);
    }

    abstract String getId();
}
