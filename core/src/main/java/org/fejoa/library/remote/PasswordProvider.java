package org.fejoa.library.remote;

import java8.util.concurrent.CompletableFuture;

import java.util.concurrent.Executor;

/**
 * To be implemented by external classes that are responsible to provide the login password.
 */
abstract public class PasswordProvider {
    /**
     * Request to be sent to the password provider
     */
    public static class Request {
        final private CompletableFuture<String> passwordFuture;
        public Request(CompletableFuture<String> passwordFuture) {
            this.passwordFuture = passwordFuture;
        }

        public void setPassword(String password) {
            passwordFuture.complete(password);
        }

        public void abort(String message) {
            passwordFuture.completeExceptionally(new Exception(message));
        }

        /**
         * Ensure that the request fails when it hasn't been handled.
         */
        @Override
        protected void finalize() throws Throwable {
            if (!passwordFuture.isDone())
                abort("No response from the password provider");
        }
    }

    final private Executor context;

    public PasswordProvider(Executor context) {
        this.context = context;
    }
    public void requestPassword(final Request request) {
        if (context != null)
            context.execute(new Runnable() {
            @Override
            public void run() {
                getPassword(request);
            }
        });
        else
            getPassword(request);
    }
    abstract protected void getPassword(Request request);
}
