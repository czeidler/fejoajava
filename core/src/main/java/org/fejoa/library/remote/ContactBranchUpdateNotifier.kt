package org.fejoa.library.remote

import java8.util.concurrent.CompletableFuture
import kotlinx.coroutines.experimental.asCoroutineDispatcher
import kotlinx.coroutines.experimental.launch
import org.fejoa.chunkstore.Config
import org.fejoa.chunkstore.HashValue
import org.fejoa.library.*
import org.fejoa.library.database.*
import org.fejoa.library.support.await
import java.lang.RuntimeException


class ContactBranchUpdateNotifier(val client: Client, val branchIndex: BranchIndex, val remote: Remote,
                                  val notificationStatus: NotificationStatus) {
    val branch: String = branchIndex.storageDir.branch

    private val branchIndexListener: StorageDir.IListener = StorageDir.IListener { onBranchIndexUpdated() }

    init {
        branchIndex.storageDir.addListener(branchIndexListener)
    }

    private fun findBranch(startsWith: String): String? {
        return branchIndex.entries.firstOrNull { it.branch.startsWith(startsWith) }
                ?.branch
    }

    private fun onBranchIndexUpdated() {
        val currentNotificationTip = notificationStatus.getNotificationStatus(remote)
        val branchTip = branchIndex.storageDir.tip
        val diff = branchIndex.storageDir.getDiff(currentNotificationTip, branchTip)
        loop@ for (change in diff.changes) {
            when (change.type) {
                DatabaseDiff.ChangeType.REMOVED -> continue@loop
                DatabaseDiff.ChangeType.ADDED,
                DatabaseDiff.ChangeType.MODIFIED -> {
                    val parts = change.path.split("/")
                    if (parts.size > 2 || parts.isEmpty())
                        continue@loop
                    var branchId = parts[0]
                    if (parts.size == 2)
                        branchId += parts[1]
                    val branch = findBranch(branchId)
                    if (branch != null)
                        updateBranch(branch)
                }
            }

        }
    }


    private fun updateBranch(branch: String) {
        val userData = client.userData
        val branchInfos = userData.branchList.getEntries(true)
        val branchInfo = branchInfos.firstOrNull { it.branch == branch }
        if (branchInfo == null)
            return

        for (contactAccess in branchInfo.contactAccessList.entries) {
            val contactId = contactAccess.contact.join()

        }

        //client.
    }
}


/**
 * Keeps track to which extend contacts have been notified about the branch status at a remote
 */
class NotificationStatus(fejoaContext: FejoaContext, val storageDir: StorageDir) {
    class Entry(val remote: String) : DBObjectContainer() {
        private val indexTip = DBHashValueObject(this, "")

        fun getTip(): HashValue? {
            return indexTip.read().join()
        }

        fun setTip(tip: HashValue) {
            this.indexTip.set(tip)
        }
    }

    val VERSION: String = "1.0"

    private val rootContainer: DBObjectContainer = DBObjectContainer(storageDir)
    private val storage: DBObjectConnector = DBObjectConnector(rootContainer, storageDir)
    private val indexVersion: DBString = DBString(rootContainer, "version")
    private val entryList = DBObjectList<Entry>(rootContainer, object : DBObjectList.SplitFileBackend<Entry>() {
        override fun create(entryName: String): Entry {
            return Entry(entryName)
        }
    })

    init {
        launch(fejoaContext.contextExecutor.asCoroutineDispatcher()) {
            val version = indexVersion.read().await()
            if (version == null) {
                indexVersion.set(VERSION)
            } else {
                if (version != VERSION)
                    throw RuntimeException("unsupported index version: " + version)
            }
        }
    }

    fun update(remote: Remote, tip: HashValue) {
        val entry = entryList.get(remote.id)
        entry.setTip(tip)
    }

    fun getNotificationStatus(remote: Remote): HashValue {
        val entry = entryList.get(remote.id)
        val hashValue = entry.getTip()
        if (hashValue != null)
            return hashValue

        return Config.newBoxHash()
    }

    fun commit(): CompletableFuture<HashValue> {
        return storage.commitAsync()
    }
}

