package org.fejoa.library.remote

import org.fejoa.chunkstore.Repository
import org.fejoa.chunkstore.sync.RequestHandler
import org.fejoa.library.Remote
import org.fejoa.library.database.StorageDir
import org.fejoa.library.support.await


class AsyncSyncer() {
    companion object Sync {
        suspend fun sync(connectionManager: ConnectionManager, storageDir: StorageDir, remote: Remote,
                                 authInfo: AuthInfo): RemoteJob.Result {
            if (storageDir.database is Repository) {
                return csSync(connectionManager, storageDir, remote, authInfo)
            } else {
                throw RuntimeException("Unsupported database")
            }
        }

        suspend fun pull(connectionManager: ConnectionManager, storageDir: StorageDir,
                 remote: Remote, authInfo: AuthInfo): ChunkStorePullJob.Result {
            val repository = storageDir.database as Repository
            val pullJob = connectionManager.submit(ChunkStorePullJob(repository, storageDir.commitSignature, remote.user,
                    repository.branch), remote, authInfo)

            // pull
            val result = pullJob.await()
            if (result.status == RequestHandler.Result.ERROR.value)
                return result

            val headCommit = repository.headCommit
            if (headCommit == null) {
                assert(result.pulledRev.dataHash.isZero)
                return result
            }

            val tip = storageDir.tip
            if (!result.pulledRev.dataHash.isZero && result.oldTip != tip)
                storageDir.onTipUpdated(result.oldTip, tip)
            return result
        }

        suspend private fun csSync(connectionManager: ConnectionManager, storageDir: StorageDir, remote: Remote,
                                   authInfo: AuthInfo): RemoteJob.Result {
            val repository = storageDir.database as Repository
            val branch = repository.branch
            val pullJob = connectionManager.submit(ChunkStorePullJob(repository, storageDir.commitSignature, remote.user,
                    repository.branch), remote, authInfo)

            // pull
            val result = pullJob.await()
            if (result.status == RequestHandler.Result.ERROR.value)
                return RemoteJob.Result(result.status, "Failed to pull: "  + branch + " " + result.message)

            val headCommit = repository.headCommit
            if (headCommit == null) {
                assert(result.pulledRev.dataHash.isZero)
                return RemoteJob.Result(Errors.DONE, "Pull: " + "Nothing to sync, tips are ZERO: " + branch)
            }

            val tip = storageDir.tip
            if (!result.pulledRev.dataHash.isZero && result.oldTip != tip)
                storageDir.onTipUpdated(result.oldTip, tip)

            if (headCommit.ref == result.pulledRev)
                return RemoteJob.Result(Errors.DONE, "Sync after pull: " + branch)

            // push
            val pushJob = connectionManager.submit(ChunkStorePushJob(repository, remote.user,
                    repository.branch), remote, authInfo)
            pushJob.await()
            return RemoteJob.Result(Errors.DONE, "sync after push: " + branch)
        }
    }
}
