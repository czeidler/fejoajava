/*
 * Copyright 2017.
 * Distributed under the terms of the GPLv3 License.
 *
 * Authors:
 *      Clemens Zeidler <czei002@aucklanduni.ac.nz>
 */
package org.fejoa.library.remote;

import java8.util.concurrent.CompletableFuture;
import org.fejoa.library.Constants;
import org.fejoa.library.FejoaContext;
import org.fejoa.library.crypto.*;
import org.json.JSONException;
import org.json.JSONObject;

import javax.crypto.SecretKey;
import java.io.IOException;
import java.io.InputStream;

import static org.fejoa.library.crypto.AuthProtocolEKE2_SHA3_256_CTR.RFC5114_2048_256;


public class LoginJob extends SimpleJsonRemoteJob<LoginJob.Result> {
    static final public String METHOD = "login";
    static final public String TYPE_KEY = "type";
    static final public String TYPE_FEJOA_EKE2_SHA3_256 = "Fejoa_EKE2_SHA3_256";
    static final public String STATE_KEY = "state";
    static final public String STATE_INIT_AUTH = "initAuth";
    static final public String STATE_FINISH_AUTH = "finishAuth";
    static final public String GP_GROUP_KEY = "encGroup";
    static final public String ENC_GX = "encX";
    static final public String ENC_GY = "encY";
    static final public String VERIFICATION_TOKEN_VERIFIER = "tokenVerifier";
    static final public String VERIFICATION_TOKEN_PROVER = "tokenProver";

    static public class Result extends RemoteJob.Result {
        final public SecretKey loginKey;

        public Result(int status, String message, SecretKey loginKey) {
            super(status, message);

            this.loginKey = loginKey;
        }
    }

    static public class InitAuthResult extends RemoteJob.Result {
        final public UserKeyParameters parameters;

        public InitAuthResult(int status, String message, UserKeyParameters parameters) {
            super(status, message);

            this.parameters = parameters;
        }
    }

    static public class InitAuthJob extends SimpleJsonRemoteJob {
        final private String userName;

        public InitAuthJob(String userName) {
            super(false);

            this.userName = userName;
        }

        @Override
        public String getJsonHeader(JsonRPC jsonRPC) throws IOException {
            return jsonRPC.call(METHOD,
                    new JsonRPC.Argument(Constants.USER_KEY, userName),
                    new JsonRPC.Argument(TYPE_KEY, TYPE_FEJOA_EKE2_SHA3_256),
                    new JsonRPC.Argument(GP_GROUP_KEY, RFC5114_2048_256),
                    new JsonRPC.Argument(STATE_KEY, STATE_INIT_AUTH));
        }

        @Override
        protected Result handleJson(JSONObject returnValue, InputStream binaryData) {
            UserKeyParameters loginUserKeyParams
                    = new UserKeyParameters(returnValue.getJSONObject(AccountSettings.LOGIN_USER_KEY_PARAMS));

            return new InitAuthResult(Errors.OK, "EKE2 auth successful", loginUserKeyParams);
        }
    }

    static public class FinishAuthJob extends SimpleJsonRemoteJob<LoginJob.Result> {
        final private String userName;
        final private SecretKey loginKey;
        final private AuthProtocolEKE2_SHA3_256_CTR.Verifier verifier;

        public FinishAuthJob(String userName, SecretKey loginKey, AuthProtocolEKE2_SHA3_256_CTR.Verifier verifier) {
            super(false);

            this.userName = userName;
            this.loginKey = loginKey;
            this.verifier = verifier;
        }

        @Override
        public String getJsonHeader(JsonRPC jsonRPC) throws IOException {
            try {
                return jsonRPC.call(METHOD,
                        new JsonRPC.Argument(Constants.USER_KEY, userName),
                        new JsonRPC.Argument(TYPE_KEY, TYPE_FEJOA_EKE2_SHA3_256),
                        new JsonRPC.Argument(STATE_KEY, STATE_FINISH_AUTH),
                        new JsonRPC.Argument(GP_GROUP_KEY, RFC5114_2048_256),
                        new JsonRPC.Argument(ENC_GY, CryptoHelper.toBase64(verifier.getEncGy())),
                        new JsonRPC.Argument(VERIFICATION_TOKEN_VERIFIER,
                                CryptoHelper.toBase64(verifier.getAuthToken())));
            } catch (CryptoException e) {
                throw new IOException(e);
            }
        }

        @Override
        protected LoginJob.Result handleJson(JSONObject returnValue, InputStream binaryData) {
            byte[] proverToken = CryptoHelper.fromBase64(returnValue.getString(VERIFICATION_TOKEN_PROVER));
            if (!verifier.verify(proverToken))
                return new LoginJob.Result(Errors.ERROR, "EKE2: server fails to send correct auth token", null);

            return new LoginJob.Result(Errors.OK, "EKE2 auth successful", loginKey);
        }
    }

    final private FejoaContext context;
    final private String userName;
    private SecretKey loginKey = null;
    final private CompletableFuture<String> password = new CompletableFuture<>();
    private PasswordProvider passwordProvider = null;
    final private InitAuthJob initAuthJob;

    public LoginJob(FejoaContext context, String userName, String password) throws CryptoException {
        super(false);

        this.context = context;
        this.userName = userName;
        if (password != null)
            this.password.complete(password);
        this.initAuthJob = new InitAuthJob(userName);
    }

    public LoginJob(FejoaContext context, String userName, SecretKey loginKey) throws CryptoException {
        this(context, userName, (String)null);

        this.loginKey = loginKey;
    }

    public LoginJob(FejoaContext context, String userName, PasswordProvider passwordProvider) throws CryptoException {
        this(context, userName, (String)null);

        setPasswordProvider(passwordProvider);
    }

    public void setPasswordProvider(PasswordProvider passwordProvider) {
        this.passwordProvider = passwordProvider;
    }

    @Override
    public String getJsonHeader(JsonRPC jsonRPC) throws IOException {
        return initAuthJob.getJsonHeader(jsonRPC);
    }

    @Override
    protected LoginJob.Result handleJson(JSONObject returnValue, InputStream binaryData) {
        try {
            InitAuthResult initAuthResult = (InitAuthResult)initAuthJob.handleJson(returnValue, binaryData);
            UserKeyParameters loginUserKeyParams = initAuthResult.parameters;

            if (loginKey == null)
                loginKey = getLoginKey(loginUserKeyParams);

            // EKE2 authenticates both sides and the server auth first. So we are the verifier and the server is the
            // prover.
            byte[] encGX = CryptoHelper.fromBase64(returnValue.getString(ENC_GX));
            AuthProtocolEKE2_SHA3_256_CTR.Verifier verifier
                    = AuthProtocolEKE2_SHA3_256_CTR.createVerifier(RFC5114_2048_256, loginKey.getEncoded(), encGX);

            setFollowUpJob(new FinishAuthJob(userName, loginKey, verifier));
            return new LoginJob.Result(Errors.FOLLOW_UP_JOB, "parameters received", null);
        } catch (JSONException e) {
            e.printStackTrace();
            return new LoginJob.Result(Errors.ERROR, "parameter missing", null);
        } catch (Exception e) {
            e.printStackTrace();
            return new LoginJob.Result(Errors.ERROR, "Exception: " + e.getMessage(), null);
        }
    }

    private SecretKey getLoginKey(UserKeyParameters loginUserKeyParams) throws CryptoException, IOException {
        SecretKey baseKey = context.getBaseKey(loginUserKeyParams.baseKeyParameters);
        if (baseKey == null) {
            if (!password.isDone()) {
                if (passwordProvider == null)
                    throw new IOException("Password required and no password provider available");
                passwordProvider.requestPassword(new PasswordProvider.Request(password));
            }
            baseKey = context.getBaseKey(loginUserKeyParams.baseKeyParameters, password.join());
        }
        return UserKeyParameters.deriveUserKey(baseKey, loginUserKeyParams);
    }
}
