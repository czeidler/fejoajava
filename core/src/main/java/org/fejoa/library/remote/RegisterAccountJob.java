/*
 * Copyright 2015.
 * Distributed under the terms of the GPLv3 License.
 *
 * Authors:
 *      Clemens Zeidler <czei002@aucklanduni.ac.nz>
 */
package org.fejoa.library.remote;

import org.fejoa.library.FejoaContext;
import org.fejoa.library.UserDataSettings;
import org.fejoa.library.crypto.*;
import org.json.JSONException;
import org.json.JSONObject;

import javax.crypto.SecretKey;
import java.io.IOException;
import java.io.InputStream;


public class RegisterAccountJob extends SimpleJsonRemoteJob<RegisterAccountJob.Result> {
    static final public String METHOD = "registerAccount";

    static final public String ACCOUNT_SETTINGS_KEY = "accountSettings";

    final private FejoaContext context;
    final private String userName;
    // either the base key or the password is null, in the former case the base key is derived from the password
    private SecretKey baseKey = null;
    final private String password;
    private SecretKey loginKey = null;

    final private UserDataSettings userDataSettings;

    static public class Result extends RemoteJob.Result {
        final public SecretKey loginKey;

        public Result(int status, String message, SecretKey loginKey) {
            super(status, message);

            this.loginKey = loginKey;
        }
    }

    public RegisterAccountJob(FejoaContext context, String userName, String password, UserDataSettings userDataSettings) {
        super(false);

        this.context = context;
        this.userName = userName;
        this.password = password;

        this.userDataSettings = userDataSettings;
    }

    public RegisterAccountJob(FejoaContext context, String userName, SecretKey baseKey,
                              UserDataSettings userDataSettings) {
        super(false);

        this.context = context;
        this.userName = userName;
        this.baseKey = baseKey;
        this.password = null;

        this.userDataSettings = userDataSettings;
    }

    @Override
    public String getJsonHeader(JsonRPC jsonRPC) throws IOException, JSONException {
        // Derive the new user key params. Reuse the kdf params from the userdata keystore (so that the kdf only needs
        // to be evaluated once).
        KDFParameters kdfParameters = userDataSettings.keyStoreSettings.keyStoreCrypto.userKeyParameters.baseKeyParameters;
        ICryptoInterface crypto = Crypto.get();
        UserKeyParameters loginUserKeyParams = new UserKeyParameters(kdfParameters, crypto.generateSalt(),
                CryptoSettings.SHA3_256);

        // derive baseKey if necessary
        if (baseKey == null) {
            try {
                baseKey = context.getBaseKey(kdfParameters, password);

            } catch (CryptoException e) {
                e.printStackTrace();
            }
        }

        // derive user key
        try {
            loginKey = UserKeyParameters.deriveUserKey(baseKey, loginUserKeyParams);
        } catch (CryptoException e) {
            e.printStackTrace();
            throw new IOException(e.getMessage());
        }

        AccountSettings accountSettings = new AccountSettings(userName, loginKey.getEncoded(), loginUserKeyParams,
                userDataSettings);
        return jsonRPC.call(METHOD, new JsonRPC.Argument(ACCOUNT_SETTINGS_KEY, accountSettings.toJson()));
    }

    @Override
    protected Result handleJson(JSONObject returnValue, InputStream binaryData) {
        RemoteJob.Result baseResults = getResult(returnValue);
        return new Result(baseResults.status, baseResults.message, loginKey);
    }
}
