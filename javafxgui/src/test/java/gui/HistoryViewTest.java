package gui;

import javafx.application.Application;
import javafx.scene.Scene;
import javafx.stage.Stage;
import org.fejoa.chunkstore.*;
import org.fejoa.gui.javafx.HistoryListView;
import org.fejoa.gui.javafx.JavaFXScheduler;
import org.fejoa.library.FejoaContext;
import org.fejoa.library.crypto.CryptoException;
import org.fejoa.library.database.StorageDir;
import org.fejoa.library.support.StorageLib;

import java.io.File;
import java.io.IOException;
import java.util.Collections;

public class HistoryViewTest extends Application {
    private final static String MAIN_DIR = "guiHistoryTest";

    public static void main(String[] args) {
        launch(args);
    }

    private void merge(StorageDir base, StorageDir branch) throws IOException, CryptoException {
        Repository repository = (Repository)base.getDatabase();
        HashValue oldTip = repository.getTip();
        Repository branch1Repo = (Repository)branch.getDatabase();
        CommitBox branch1Commit = branch1Repo.getHeadCommit();
        repository.merge(branch1Repo.getCurrentTransaction(), branch1Commit);
        repository.commitInternal("Merge.", null,
                Collections.singletonList(branch1Commit.getRef()));
        base.onTipUpdated(oldTip, repository.getHeadCommit().getPlainHash());
    }

    private StorageDir buildHistory(FejoaContext context, StorageDir storageDir) throws IOException, CryptoException {
        String branch = storageDir.getBranch();

        storageDir.writeString("test", "test");
        storageDir.commit("commit 1");
        storageDir.writeString("test2", "test2");
        storageDir.commit("commit 2");
        HashValue tipCommit2 = storageDir.getTip();
        storageDir.writeString("test3", "test3");
        storageDir.commit("commit 3");
        HashValue tipCommit3 = storageDir.getTip();
        storageDir.writeString("test4", "test4");
        storageDir.commit("commit 4");


        // branch of a branch at commit 2
        StorageDir branch1 = context.getStorage(branch, tipCommit2, null, null);
        branch1.writeString("testB1", "From Branch 1");
        branch1.commit("branch 1 commit");
        branch1.writeString("testB12", "data");
        branch1.commit("branch 1 commit");

        merge(storageDir, branch1);

        // branch of a branch at commit 2
        StorageDir branch2 = context.getStorage(branch, tipCommit3, null, null);
        branch2.writeString("testB2", "From Branch 2");
        branch2.commit("branch 2 commit");
        branch2.writeString("testB22", "From Branch 2");
        branch2.commit("branch 2 commit");

        merge(storageDir, branch2);
        return storageDir;
    }

    @Override
    public void start(Stage stage) throws Exception {
        StorageLib.recursiveDeleteFile(new File(MAIN_DIR));
        FejoaContext context = new FejoaContext(MAIN_DIR, new JavaFXScheduler());
        StorageDir storageDir = context.getStorage("test", null, null);

        HistoryListView historyView = new HistoryListView(storageDir);

        buildHistory(context, storageDir);

        System.out.println(historyView.getHistoryList());

        stage.setScene(new Scene(historyView));
        stage.show();
    }

}
