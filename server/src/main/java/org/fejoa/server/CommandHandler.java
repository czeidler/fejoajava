/*
 * Copyright 2015.
 * Distributed under the terms of the GPLv3 License.
 *
 * Authors:
 *      Clemens Zeidler <czei002@aucklanduni.ac.nz>
 */
package org.fejoa.server;

import org.apache.commons.io.IOUtils;
import org.apache.commons.io.output.ByteArrayOutputStream;
import org.fejoa.chunkstore.ChunkStoreBranchLog;
import org.fejoa.chunkstore.HashValue;
import org.fejoa.library.BranchIndex;
import org.fejoa.library.Constants;
import org.fejoa.library.FejoaContext;
import org.fejoa.library.command.IncomingCommandQueue;
import org.fejoa.library.database.StorageDir;
import org.fejoa.library.remote.Errors;
import org.fejoa.library.remote.JsonRPCHandler;
import org.fejoa.library.remote.SendCommandJob;

import java.io.IOException;
import java.io.InputStream;
import java.util.Collection;


public class CommandHandler extends JsonRequestHandler {
    public CommandHandler() {
        super(SendCommandJob.METHOD);
    }

    @Override
    public void handle(Portal.ResponseHandler responseHandler, JsonRPCHandler jsonRPCHandler, InputStream data,
                       Session session) throws Exception {
        if (data == null)
            throw new IOException("Command data expected");

        String serverUser = jsonRPCHandler.getParams().getString(Constants.SERVER_USER_KEY);

        ByteArrayOutputStream outputStream = new ByteArrayOutputStream();
        IOUtils.copy(data, outputStream);

        final FejoaContext context = session.getContext(serverUser);
        IncomingCommandQueue queue = session.getIncomingCommandQueue(serverUser);
        final StorageDir queueStorageDir = queue.getStorageDir();
        final HashValue initialHead = context.getStorageLogTip(queueStorageDir);
        queue.addCommand(outputStream.toByteArray());
        queue.commit();

        // update the server branch index if necessary
        final HashValue finalHead = context.getStorageLogTip(queueStorageDir);
        if ((initialHead == null && finalHead != null) || (initialHead != null && finalHead != null
                && !initialHead.equals(finalHead))) {
            ServerBranchIndex index = session.getServerBranchIndex(serverUser);
            index.updateBranch(queueStorageDir.getBranch(), finalHead);
        }

        String response = jsonRPCHandler.makeResult(Errors.OK, "command delivered");
        responseHandler.setResponseHeader(response);
    }
}
