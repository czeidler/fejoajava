package org.fejoa.server

import kotlinx.coroutines.experimental.asCoroutineDispatcher
import kotlinx.coroutines.experimental.runBlocking
import org.fejoa.chunkstore.ChunkStoreBranchLog
import org.fejoa.chunkstore.HashValue
import org.fejoa.library.BranchIndex
import org.fejoa.library.FejoaContext
import org.fejoa.library.crypto.CryptoHelper
import java.io.File
import java.util.*
import java.util.concurrent.locks.Lock
import java.util.concurrent.locks.ReentrantLock


class ServerBranchIndex(val fejoaContext: FejoaContext) {
    private var branchIndexVar: BranchIndex? = null

    fun getBranchIndex(): BranchIndex {
        return lockOperation {
            getBranchIndexUnlocked()
        }
    }

    fun updateBranch(branch: String, tip: HashValue) {
        updateBranches(Collections.singletonList(Pair(branch, tip)))
    }

    fun getBranchIndexDir(): File {
        val chunkStoreFile = fejoaContext.chunkStoreDir
        return File(chunkStoreFile, "branchIndex")
    }

    private fun updateBranches(branches: List<Pair<String, HashValue>>) {
        lockOperation {
            updateBranchesUnlocked(branches)
        }
    }

    private fun getBranchIndexUnlocked(): BranchIndex {
        if (branchIndexVar == null) {
            val indexFile = getBranchIndexDir()
            val branches = ChunkStoreBranchLog.listLocalBranches(indexFile)
            val remoteIndexName = branches.firstOrNull()
            val branchName: String

            if (remoteIndexName == null)
                branchName = CryptoHelper.generateSha1Id(fejoaContext.crypto)
            else
                branchName = remoteIndexName.branch

            val storageDir = fejoaContext.getPlainStorage(indexFile, branchName)
            branchIndexVar = BranchIndex(storageDir)

            if (remoteIndexName == null)
                updateAllBranchesUnlocked()
        }
        return branchIndexVar!!
    }

    private fun updateAllBranchesUnlocked() {
        val localBranches = ChunkStoreBranchLog.listLocalBranches(fejoaContext.chunkStoreDir)
        val branchTips = localBranches
                .filter { it.head != null }
                .map { Pair(it.branch, it.head.entryId) }

        updateBranchesUnlocked(branchTips)
    }

    private fun updateBranchesUnlocked(branches: List<Pair<String, HashValue>>) {
        val branchIndex = getBranchIndexUnlocked()
        for ((first, second) in branches)
            branchIndex.update(first, second)
        branchIndex.commit()
    }

    private fun <T> lockOperation(block: () -> T): T {
        return runBlocking(fejoaContext.contextExecutor.asCoroutineDispatcher()) {
            block.invoke()
        }
    }
}
