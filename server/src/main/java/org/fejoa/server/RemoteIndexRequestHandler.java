package org.fejoa.server;

import org.fejoa.library.Constants;
import org.fejoa.library.remote.Errors;
import org.fejoa.library.remote.GetRemoteIndexJob;
import org.fejoa.library.remote.JsonRPC;
import org.fejoa.library.remote.JsonRPCHandler;
import org.json.JSONObject;

import java.io.InputStream;


public class RemoteIndexRequestHandler extends JsonRequestHandler {
    public RemoteIndexRequestHandler() {
        super(GetRemoteIndexJob.METHOD);
    }

    @Override
    public void handle(Portal.ResponseHandler responseHandler, JsonRPCHandler jsonRPCHandler, InputStream data, Session session) throws Exception {
        JSONObject params = jsonRPCHandler.getParams();
        String user = params.getString(Constants.SERVER_USER_KEY);
        AccessControl accessControl = new AccessControl(session, user);
        if (!accessControl.isRootUser()) {
            responseHandler.setResponseHeader(jsonRPCHandler.makeResult(Errors.ACCESS_DENIED,
                    "insufficient rights"));
            return;
        }
        ServerBranchIndex index = session.getServerBranchIndex(user);
        String branchName = index.getBranchIndex().getIndexName();

        responseHandler.setResponseHeader(jsonRPCHandler.makeResult(Errors.OK, "ok",
                new JsonRPC.Argument(Constants.BRANCH_KEY, branchName)));
    }

}
