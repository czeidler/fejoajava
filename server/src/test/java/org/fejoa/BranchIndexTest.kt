package org.fejoa

import junit.framework.TestCase
import kotlinx.coroutines.experimental.Job
import kotlinx.coroutines.experimental.delay
import kotlinx.coroutines.experimental.launch
import kotlinx.coroutines.experimental.runBlocking
import org.fejoa.library.*
import org.fejoa.library.command.AccessCommand
import org.fejoa.library.command.AccessCommandHandler
import org.fejoa.library.remote.RemoteIndexSyncManager
import org.fejoa.library.remote.TaskUpdate
import org.fejoa.library.support.LooperExecutor
import org.fejoa.library.support.StorageLib
import org.fejoa.library.support.Task
import org.fejoa.library.support.await
import org.fejoa.server.CookiePerPortManager
import org.fejoa.server.JettyServer
import org.junit.After
import org.junit.Before
import org.junit.Test
import java.io.File
import java.net.CookieHandler
import java.net.CookiePolicy
import java.util.ArrayList
import java.util.concurrent.Executors


class BranchIndexTest {
    private inner class ClientStatus(val name: String, val server: String) {
        val remote: Remote = Remote(name, server)
    }

    internal val TEST_DIR = "branchIndexTest"
    internal val SERVER_TEST_DIR_1 = TEST_DIR + "/Server1"
    internal val SERVER_URL_1 = "http://localhost:${JettyServer.DEFAULT_PORT}/"
    internal val USER_NAME_1 = "testUser1"
    internal val PASSWORD = "password"

    private val cleanUpDirs = ArrayList<String>()
    private lateinit var server1: JettyServer
    private lateinit var client1: Client
    private lateinit var clientStatus1: ClientStatus


    private val clientThread = LooperExecutor(Executors.newSingleThreadExecutor())

    private lateinit var mainJob: Job

    @Before
    fun setUp() {
        val executor = clientThread

        cleanUpDirs.add(TEST_DIR)
        for (dir in cleanUpDirs)
            StorageLib.recursiveDeleteFile(File(dir))

        // allow cookies per port number in order so run multiple servers on localhost
        CookieHandler.setDefault(CookiePerPortManager(null, CookiePolicy.ACCEPT_ALL))

        server1 = JettyServer(SERVER_TEST_DIR_1, JettyServer.DEFAULT_PORT)
        server1.start()

        clientStatus1 = ClientStatus(USER_NAME_1, SERVER_URL_1)

        client1 = Client.init(File(TEST_DIR + "/" + USER_NAME_1), executor, PASSWORD)
        client1.commit()
        client1.getConnectionManager().setStartScheduler(Task.NewThreadScheduler())
        client1.getConnectionManager().setObserverScheduler(clientThread)

        val accessHandler = client1.getIncomingCommandManager().getHandler(
                AccessCommand.COMMAND_NAME) as AccessCommandHandler
        accessHandler.addContextHandler(UserData.USER_DATA_CONTEXT) { senderId, branchInfo -> true }
    }

    @Throws(Exception::class)
    @After
    fun tearDown() {
        server1.stop()

        for (dir in cleanUpDirs)
            StorageLib.recursiveDeleteFile(File(dir))
    }

    suspend private fun createAndSyncAccountTask(client: Client, status: ClientStatus) {
        val createJob = client.registerAccount(status.remote, PASSWORD)
        val createResult = createJob.await()

        val addRemoteJob = client.addGateWayRemote(status.remote, createResult.loginKey)
        addRemoteJob.await()

        println("Account Created")
    }

    private fun finishAndFail(message: String?) {
        mainJob.cancel()
        TestCase.fail(message)
    }

    @Test
    fun testClient() {
        runBlocking {
            mainJob = Job()
            val createClient1 = launch(context) {
                createAndSyncAccountTask(client1, clientStatus1)
            }
            createClient1.join()

            val syncer = RemoteIndexSyncManager(client1, object: Task.IObserver<TaskUpdate, Void> {
                override fun onResult(result: Void?) {

                }

                override fun onException(exception: java.lang.Exception?) {

                }

                override fun onProgress(update: TaskUpdate?) {

                }

            })
            syncer.startWatching(client1.userData.remoteStore.entries)

            // clean up
            delay(1000 * 60)
            client1.stopSyncing()
        }
    }
}

