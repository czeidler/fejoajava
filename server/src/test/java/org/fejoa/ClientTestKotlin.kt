package org.fejoa

import kotlinx.coroutines.experimental.*
import org.fejoa.library.*
import org.fejoa.library.remote.Errors
import org.fejoa.library.remote.TaskUpdate
import org.fejoa.library.support.LooperExecutor
import org.fejoa.library.support.StorageLib
import org.fejoa.library.support.Task
import org.fejoa.library.support.await
import org.fejoa.server.CookiePerPortManager
import org.fejoa.server.JettyServer
import org.fejoa.library.UserData.USER_DATA_CONTEXT
import org.fejoa.library.command.*
import org.fejoa.library.remote.RemoteJob
import org.fejoa.server.JettyServer.DEFAULT_PORT
import junit.framework.TestCase
import org.junit.After
import org.junit.Before
import org.junit.Test
import java.util.ArrayList
import java.io.File
import java.io.IOException
import java.net.CookieHandler
import java.net.CookiePolicy
import java8.util.concurrent.CompletableFuture
import java.util.concurrent.Executors


class ClientTestKotlin {
    private inner class ClientStatus(val name: String, val server: String) {
        val remote: Remote = Remote(name, server)
        var firstSync: Boolean = false
    }

    internal val TEST_DIR = "jettyTest"
    internal val SERVER_TEST_DIR_1 = TEST_DIR + "/Server1"
    internal val SERVER_TEST_DIR_2 = TEST_DIR + "/Server2"
    internal val SERVER_TEST_DIR_3 = TEST_DIR + "/Server3"
    internal val SERVER_URL_1 = "http://localhost:$DEFAULT_PORT/"
    internal val SERVER_URL_2 = "http://localhost:" + (DEFAULT_PORT + 1) + "/"
    internal val USER_NAME_1 = "testUser1"
    internal val USER_NAME_1_NEW = "testUser1New"
    internal val SERVER_URL_1_NEW = "http://localhost:" + (DEFAULT_PORT + 2) + "/"
    internal val USER_NAME_2 = "testUser2"
    internal val PASSWORD = "password"

    private val cleanUpDirs = ArrayList<String>()
    private lateinit var server1: JettyServer
    private lateinit var client1: Client
    private lateinit var clientStatus1: ClientStatus

    private lateinit var server2: JettyServer
    private lateinit var client2: Client
    private lateinit var clientStatus2: ClientStatus

    private lateinit var serverNew: JettyServer
    private val clientThread = LooperExecutor(Executors.newSingleThreadExecutor())

    private lateinit var mainJob: Job

    @Before
    fun setUp() {
        cleanUpDirs.add(TEST_DIR)
        for (dir in cleanUpDirs)
            StorageLib.recursiveDeleteFile(File(dir))

        // allow cookies per port number in order so run multiple servers on localhost
        CookieHandler.setDefault(CookiePerPortManager(null, CookiePolicy.ACCEPT_ALL))

        server1 = JettyServer(SERVER_TEST_DIR_1, DEFAULT_PORT)
        server1.start()
        server2 = JettyServer(SERVER_TEST_DIR_2, DEFAULT_PORT + 1)
        server2.start()
        serverNew = JettyServer(SERVER_TEST_DIR_3, DEFAULT_PORT + 2)
        serverNew.start()

        clientStatus1 = ClientStatus(USER_NAME_1, SERVER_URL_1)
        clientStatus2 = ClientStatus(USER_NAME_2, SERVER_URL_2)

        client1 = Client.init(File(TEST_DIR + "/" + USER_NAME_1), clientThread, PASSWORD)
        client1.commit()

        val workExecutor = Task.NewThreadScheduler()
        client1.getConnectionManager().setStartScheduler(workExecutor)
        client1.getConnectionManager().setObserverScheduler(clientThread)

        client2 = Client.init(File(TEST_DIR + "/" + USER_NAME_2), clientThread, PASSWORD)
        client2.commit()
        client2.getConnectionManager().setStartScheduler(workExecutor)
        client2.getConnectionManager().setObserverScheduler(clientThread)

        val accessHandler = client1.getIncomingCommandManager().getHandler(
                AccessCommand.COMMAND_NAME) as AccessCommandHandler
        accessHandler.addContextHandler(UserData.USER_DATA_CONTEXT) { senderId, branchInfo -> true }
    }

    @Throws(Exception::class)
    @After
    fun tearDown() {
        server1.stop()

        for (dir in cleanUpDirs)
            StorageLib.recursiveDeleteFile(File(dir))
    }


    suspend private fun createAndSyncAccountTask(client: Client, status: ClientStatus) {
        val createJob = client.registerAccount(status.remote, PASSWORD)
        val createResult = createJob.await()
        println("Account Created")

        val addRemoteJob = client.addGateWayRemote(status.remote, createResult.loginKey)
        addRemoteJob.await()
        println("Remote gateway added")

        // watch
        val firstSyncJob = CompletableFuture<Void>()
        client.startSyncing(object : Task.IObserver<TaskUpdate, Void> {
            override fun onProgress(update: TaskUpdate) {
                assert(Thread.currentThread().id == clientThread.currentThreadId)
                println(update.toString())
                if (!status.firstSync && update.totalWork > 0 && update.progress == update.totalWork) {
                    status.firstSync = true
                    firstSyncJob.complete(null)
                }
            }

            override fun onResult(aVoid: Void?) {
                assert(Thread.currentThread().id == clientThread.currentThreadId)
                println(status.name + ": sync ok")
            }

            override fun onException(exception: Exception) {
                assert(Thread.currentThread().id == clientThread.currentThreadId)
                if (exception.message == "canceled")
                    return
                exception.printStackTrace()
                firstSyncJob.completeExceptionally(exception)
            }
        })
        firstSyncJob.await()

        // start and setup commands
        client.startCommandManagers(object : Task.IObserver<TaskUpdate, Void> {
            override fun onProgress(update: TaskUpdate) {
                println(status.name + ": " + update.toString())
            }

            override fun onResult(aVoid: Void?) {
                println(status.name + ": Command sent")
            }

            override fun onException(exception: Exception) {
                finishAndFail(exception)
            }
        })
        val handler = client.incomingCommandManager
                .getHandler(ContactRequestCommand.COMMAND_NAME) as ContactRequestCommandHandler
        handler.listener = object : ContactRequestCommandHandler.AutoAccept() {
            override fun onError(exception: Exception) {
                exception.printStackTrace()
                finishAndFail(exception)
            }
        }
    }

    private fun finishAndFail(exception: Exception) {
        exception.printStackTrace()
        finishAndFail(exception.message)
    }

    private fun finishAndFail(message: String?) {
        mainJob.cancel()
        TestCase.fail(message)
    }

    internal inner class GrantAccessForClient1Task {
        suspend fun perform() {
            val accessGrantedJob = CompletableFuture<Void>()
            val handler = client1.incomingCommandManager
                    .getHandler(AccessCommand.COMMAND_NAME) as AccessCommandHandler
            handler.listener = object : AccessCommandHandler.IListener {
                override fun onError(e: Exception) {
                    accessGrantedJob.completeExceptionally(e)
                }

                override fun onAccessGranted(contactId: String, accessTokenContact: AccessTokenContact) {
                    println("Access granted: " + contactId + " access entry: "
                            + accessTokenContact.accessEntry)
                    accessGrantedJob.complete(null)
                }
            }

            val clientUserData = client2.userData
            val contactStore = clientUserData.contactStore
            val client2Contact = contactStore.contactList.get(
                    client1.userData.myself.id)

            // grant access to the access branch
            val branch = clientUserData.accessStore.id
            println("Client2 grant access for:" + branch)
            client2.grantAccess(branch, USER_DATA_CONTEXT, BranchAccessRight.PULL, client2Contact)
            accessGrantedJob.await()

            var uploaded = false
            for (retryCount in 0..9) {
                if (checkClient2UploadedTheAccessStore()) {
                    uploaded = true
                    println("Access store updated, retry counts: " + retryCount)
                    break
                }
                delay(500)
            }
            if (!uploaded)
                finishAndFail("failed to check access store status: too many retries")

            handler.listener = null
        }

        suspend private fun checkClient2UploadedTheAccessStore(): Boolean {
            val userData = client2.userData
            val accessBranchInfo = userData.findBranchInfo(userData.accessStore.id, USER_DATA_CONTEXT)
            val peekJob = client2.peekRemoteStatus(accessBranchInfo.locationEntries.iterator().next())
            val result = peekJob.await()

            if (result.status != Errors.DONE || result.updated == null) {
                finishAndFail(result.message)
                return false
            }

            val localLogTip = userData.context.getStorageLogTip(accessBranchInfo.branch)
            for (branchLogTip in result.updated) {
                if (localLogTip == branchLogTip.logTip)
                    return true
            }
            return false
        }
    }

    suspend fun sendContactRequest() {
        val job = CompletableFuture<Void>()
        val handler = client1.incomingCommandManager
                .getHandler(ContactRequestCommand.COMMAND_NAME) as ContactRequestCommandHandler
        handler.listener = object : ContactRequestCommandHandler.AutoAccept() {
            override fun onContactRequestReply(contactRequest: ContactRequestCommandHandler.ContactRequest) {
                super.onContactRequestReply(contactRequest)
                assert(Thread.currentThread().id == clientThread.currentThreadId)
                job.complete(null)
            }

            override fun onError(exception: Exception) {
                assert(Thread.currentThread().id == clientThread.currentThreadId)
                job.completeExceptionally(exception)
            }
        }
        client1.contactRequest(USER_NAME_2, SERVER_URL_2)
        job.await()
    }

    suspend fun pullBranchFromClient2() {
        val clientUserData = client1.userData
        val contactStore = clientUserData.contactStore
        val client2Contact = contactStore.contactList.get(
                client2.userData.myself.id)

        val contactBranch = client2Contact.branchList.entries.iterator().next()
        val location = contactBranch.locationEntries.iterator().next()

        val pullJob = client1.pullBranch(Remote(USER_NAME_2, SERVER_URL_2), location)
        pullJob.await()
        TestCase.assertFalse(client1.context.getPlainStorage(contactBranch.branch).tip.isZero)
    }

    suspend fun migrateClient1() {
        val migrateJob = CompletableFuture<Void>()

        val createNewAccountJob = client1.registerAccount(Remote(USER_NAME_1_NEW, SERVER_URL_1_NEW), PASSWORD)
        val createAccountResult = createNewAccountJob.await()

        val handler = client2.incomingCommandManager
                .getHandler(MigrationCommand.COMMAND_NAME) as MigrationCommandHandler
        handler.listener = object : MigrationCommandHandler.IListener {
            override fun onError(e: Exception) {
                assert(Thread.currentThread().id == clientThread.currentThreadId)
                migrateJob.completeExceptionally(e)
            }

            override fun onContactMigrated(contactId: String) {
                assert(Thread.currentThread().id == clientThread.currentThreadId)
                println("Contact migrated: " + contactId)

                val contactPublic = client2.userData.contactStore
                        .contactFinder.get(contactId) as ContactPublic
                val newRemote = contactPublic.remotes.default
                TestCase.assertEquals(USER_NAME_1_NEW, newRemote.user)
                TestCase.assertEquals(SERVER_URL_1_NEW, newRemote.server)

                val handler = client2.incomingCommandManager
                        .getHandler(MigrationCommand.COMMAND_NAME) as MigrationCommandHandler
                handler.listener = null
                migrateJob.complete(null)
            }
        }

        val migrationManager = client1.migrationManager
        val remote = client1.userData.gateway
        migrationManager.migrate(Remote(remote.id, USER_NAME_1_NEW, SERVER_URL_1_NEW), createAccountResult.loginKey,
                object : Task.IObserver<Void, RemoteJob.Result> {
                    override fun onProgress(aVoid: Void) {
                        assert(Thread.currentThread().id == clientThread.currentThreadId)
                    }

                    override fun onResult(result: RemoteJob.Result) {
                        assert(Thread.currentThread().id == clientThread.currentThreadId)
                        try {
                            val newGateway = client1.userData.gateway
                            TestCase.assertEquals(remote.id, newGateway.id)
                            TestCase.assertEquals(USER_NAME_1_NEW, newGateway.user)
                            TestCase.assertEquals(SERVER_URL_1_NEW, newGateway.server)
                        } catch (e: IOException) {
                            migrateJob.completeExceptionally(e)
                        }

                    }

                    override fun onException(exception: Exception) {
                        assert(Thread.currentThread().id == clientThread.currentThreadId)
                        val handler = client2.incomingCommandManager
                                .getHandler(MigrationCommand.COMMAND_NAME) as MigrationCommandHandler
                        handler.listener = null
                        migrateJob.completeExceptionally(exception)
                    }
                })

        migrateJob.await()
    }

    @Test
    fun testClient() {
        runBlocking(clientThread.asCoroutineDispatcher()) {
            mainJob = Job()
            val createClient1 = launch(clientThread.asCoroutineDispatcher()) {
                createAndSyncAccountTask(client1, clientStatus1)
            }
            val createClient2 = launch(clientThread.asCoroutineDispatcher()) {
                createAndSyncAccountTask(client2, clientStatus2)
            }
            createClient1.join()
            createClient2.join()

            println("Send contact request from client1 to client2:")
            sendContactRequest()
            println("Grant access client2 -> client1:")
            GrantAccessForClient1Task().perform()
            println("Pull branch from client2 to client1:")
            pullBranchFromClient2()
            println("Start migration:")
            migrateClient1()

            // clean up
            client1.stopSyncing()
            client2.stopSyncing()
        }
    }
}